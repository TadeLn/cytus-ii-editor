import os
import json

data = {
    "songs": {},
    "collections": {}
}
os.chdir("chart")

for root, directories, files in os.walk(".", followlinks = True):
    for filename in files:
        if filename == "info.json":
            song_data = {}

            try:
                with open(os.path.join(root, filename), "r") as f:
                    song_data = json.load(f)
            except Exception as ex:
                print("An error occurred: " + str(ex))
                pass

            song_id = song_data["id"]

            data["songs"][root] = song_data
            collection_id = "other"

            segments = song_id.split('_')
            if len(segments) > 1:
                # Check if 3 last chars can be converted to a number
                try:
                    int(segments[0][-3:])
                    collection_id = segments[0]
                except:
                    pass

                if segments[1] == "bossstage":
                    collection_id = "bossstage"

            if collection_id not in data["collections"].keys():
                data["collections"][collection_id] = []

            data["collections"][collection_id].append(root)
            # print(root)

with open("../chartlist.json", "w+") as f:
    json.dump(data, f, indent=4)



print("Done")
