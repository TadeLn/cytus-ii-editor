import App from "../App.js";
import Screen from "./Screen.js";

export default class SettingsScreen extends Screen {
  override open(): void {
    document.getElementById("settings-screen").classList.add("active");
    (<HTMLElement> document.getElementById("song-select-screen").querySelector("#close-btn")).onclick = () => {
      App.i.screenManager.pop();
    }
  }

  override close(): void {
    document.getElementById("settings-screen").classList.remove("active");
  }
}