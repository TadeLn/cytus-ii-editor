import App from "../App.js";
import Screen from "./Screen.js";

export default class HelpScreen extends Screen {
  override open(): void {
    document.getElementById("help-screen").classList.add("active");
    (<HTMLElement> document.getElementById("help-screen").querySelector("#close-btn")).onclick = () => {
      App.i.screenManager.pop();
    }
  }

  override close(): void {
    document.getElementById("help-screen").classList.remove("active");
  }

  override onKeyDown(code: string): boolean {
    if (code === "F1" || code === "Escape") {
      App.i.screenManager.pop();
      return true;
    }
    return false;
  }
}