import Screen from "./Screen.js";

export default class ScreenManager {

  private screenStack = new Array<Screen>();
  
  get currentScreen(): Screen | null {
    return this.screenStack.length === 0 ? null : this.screenStack[this.screenStack.length - 1];
  }

  push(screen: Screen) {
    if (this.currentScreen !== null) {
      this.currentScreen.suspend();
    }
    this.screenStack.push(screen);
    this.currentScreen.open();
  }

  pop() {
    if (this.currentScreen !== null) {
      this.currentScreen.close();
      this.screenStack.pop();
    }
    if (this.currentScreen !== null) {
      this.currentScreen.resume();
    }
  }

  replace(screen: Screen) {
    if (this.currentScreen !== null) {
      this.currentScreen.close();
      this.screenStack.pop();
    }
    this.screenStack.push(screen);
    this.currentScreen.open();
  }

  closeAll() {
    while (this.currentScreen !== null) {
      this.pop();
    }
  }

};
