import App from "../App.js";
import { SongSource } from "../util/URLHash.js";
import Screen from "./Screen.js";

export default class SongSelectScreen extends Screen {
  override open(): void {
    const songSelectScreen = document.getElementById("song-select-screen");
    songSelectScreen.classList.add("active");
    (<HTMLElement> songSelectScreen.querySelector("#close-btn")).onclick = () => {
      App.i.screenManager.pop();
    }
    songSelectScreen.focus();

    const chart = App.i.currentChart;
    if (chart !== null) {
      if (chart.source === SongSource.SERVER) {
        document.querySelector(`#song-select-screen .song-selected .anchor`).scrollIntoView();
      }
    }
  }

  override close(): void {
    document.getElementById("song-select-screen").classList.remove("active");
  }

  override onKeyDown(code: string): boolean {
    if (code === "KeyP" || code === "Escape") {
      App.i.screenManager.pop();
      return true;
    }
    return false;
  }
}