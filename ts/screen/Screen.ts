export default abstract class Screen {

  // Called when the screen is created
  open(): void {};

  // Called when the screen needs to be closed
  close(): void {};

  // Called when another screen has been opened on top of this one
  suspend(): void {};

  // Called when the screen on top has been closed
  resume(): void {};

  
  // Return true if the event should not be propagated anymore
  onKeyDown(code: string): boolean { return false };

  // Return true if the event should not be propagated anymore
  onKeyUp(code: string): boolean { return false };

  
  // Return true if the event should not be propagated anymore
  onMouseMove(x: number, y: number): boolean { return false };

  // Return true if the event should not be propagated anymore
  onMouseDown(x: number, y: number, button: number): boolean { return false };

  // Return true if the event should not be propagated anymore
  onMouseUp(x: number, y: number, button: number): boolean { return false };

  // Return true if the event should not be propagated anymore
  onMouseWheel(x: number, y: number, deltaY: number): boolean { return false };

};
