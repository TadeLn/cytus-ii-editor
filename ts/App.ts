import Chart from "./chart/Chart.js";
import ChartList from "./chart/ChartList.js";
import NoteType from "./chart/NoteType.js";
import Keyboard from "./input/Keyboard.js";
import Mouse from "./input/Mouse.js";
import Touch from "./input/Touch.js";
import MusicManager from "./music/MusicManager.js";
import { default as R } from "./renderer/Renderer.js";
import Style from "./renderer/Style.js";
import ScreenManager from "./screen/ScreenManager.js";
import SongSelectScreen from "./screen/SongSelectScreen.js";
import Settings from "./Settings.js";
import State from "./State.js";
import NoteTool from "./tool/NoteTool.js";
import Tool from "./tool/Tool.js";
import Toolbar from "./tool/Toolbar.js";
import GETRequest from "./util/request/GETRequest.js";
import URLHash, { SongSource } from "./util/URLHash.js";
import Util from "./util/Util.js";

class App {
  tick = 0;
  tps = 0;
  lastLoop = Date.now();

  countFps(): void {
    this.tps = this.tick;
    this.tick = 0;

    R.i.fps = R.i.frame;
    R.i.frame = 0;

    R.i.setDrawPlayfield();
  }

  currentChart: Chart = null;

  state = new State();
  style = new Style();

  urlHash = new URLHash();
  screenManager = new ScreenManager();

  get currentScreen() {
    return this.screenManager.currentScreen;
  }


  // Sets this.state.currentTick and the music offset
  setTick(tick = this.state.currentTick) {
    this.state.currentTick = tick;
    if (this.currentChart !== null) {
      MusicManager.seek(this.currentChart.tickToMs(this.state.currentTick));
    }
  }

  previousStep(step = this.currentChart.timeBase / 4) {
    this.setTick(Math.floor((this.state.currentTick - 1) / step) * step);
    R.i.setDrawPlayfield();
  }

  nextStep(step = this.currentChart.timeBase / 4) {
    this.setTick(Math.ceil((this.state.currentTick + 1) / step) * step);
    R.i.setDrawPlayfield();
  }

  zoomIn() {
    if (!this.state.gameplayMode) this.state.zoomExponent++;
  }

  zoomOut() {
    if (!this.state.gameplayMode) this.state.zoomExponent--;
  }

  changeTool(newTool: Tool | null) {
    if (this.state.selectedTool !== null) {
      this.state.selectedTool.destruct();
    }
    this.state.selectedTool = newTool;
    R.i.setDrawPlayfield();
  }


  selectNoteType(type: NoteType) {
    if (!(this.state.selectedTool instanceof NoteTool)) {
      this.changeTool(new NoteTool());
    }
    (<NoteTool>this.state.selectedTool).changeNoteType(type);
  }



  unloadChart() {
    MusicManager.audio.pause();
    this.state.currentTick = 0;
    this.currentChart = null;

    const selectedSongRows = document.getElementById("song-select-screen").getElementsByClassName("song-selected");
    for (let i = 0; i < selectedSongRows.length; i++) {
      selectedSongRows[i].classList.remove("song-selected");
    }

    const selectedSongDifficulties = document.getElementById("song-select-screen").getElementsByClassName("difficulty-selected");
    for (let i = 0; i < selectedSongDifficulties.length; i++) {
      selectedSongDifficulties[i].classList.remove("difficulty-selected");
    }
  }

  loadChart(chart: Chart, audioFilename: string) {
    this.currentChart = chart;
    MusicManager.audio = new Audio(audioFilename);
    MusicManager.audio.loop = false;
    MusicManager.audio.playbackRate = 1;
    this.urlHash.updateHash();
    this.screenManager.closeAll();

    if (chart.source === SongSource.SERVER) {
      document.getElementById(`songlist-song-${chart.filename}`).classList.add("song-selected");
      document.getElementById(`songlist-difficulty-${chart.filename}-${chart.difficulty}`).classList.add("difficulty-selected");
    }
    this.state.currentTick = this.state.currentTick;

    R.i.updateSize();
    R.i.setDrawPlayfield();
  }

  // Download chart info and set music source
  async loadFileFromServer(filename: string, difficulty = 0) {
    this.unloadChart();

    if (filename === "") {
      this.screenManager.push(new SongSelectScreen())
    } else {
      const req = new GETRequest(`./data/chart/${filename}/chart_${difficulty}.json`);
      const res = await req.send();
      const chart = Chart.loadFromData(SongSource.SERVER, filename, difficulty, JSON.parse(res));
      if (chart !== null) {
        const musicFilename = ChartList.getMusicFilename(filename, difficulty);
        this.loadChart(chart, `./data/chart/${filename}/${musicFilename}`);
      } else {
        this.screenManager.push(new SongSelectScreen());
      }
    }
  }

  // Uncompress chart info and set music source
  loadFileFromCompressedData(compressedSongData: string) {
    console.log("loadFileFromCompressedData: compressedSongData", compressedSongData);
    const uncompressedJSON = Util.lzwDecompress64(compressedSongData);
    console.log("loadFileFromCompressedData: uncompressedJSON", uncompressedJSON);
    const uncompressedData = JSON.parse(uncompressedJSON);
    console.log("loadFileFromCompressedData: uncompressedData", uncompressedData);

    this.unloadChart();
    const chart = Chart.fromC2Min(uncompressedData.chartData);
    if (chart !== null) {
      this.loadChart(chart, uncompressedData.music);
    } else {
      this.screenManager.push(new SongSelectScreen());
    }
  }



  // Load chart and change URL
  async changeFileFromServer(filename?: string, diff = 0, timestamp: number | null = null) {
    if (filename === undefined) {
      filename = "empty";
    }
    this.urlHash.setHash(SongSource.SERVER, filename, diff, null, timestamp);
    await this.loadFileFromServer(filename, diff);
  }

  // Load chart and change URL
  async changeDiffFromServer(i: number, timestamp: number | null = null) {
    let diff = ChartList.getDiffByIndex(i);
    console.log("Changing diff", i, diff);
    if (diff !== undefined) {
      await this.changeFileFromServer(this.currentChart.filename, diff, timestamp);
    }
  }

  // Load chart and change URL
  changeFileFromCompressedData(compressedData: string, timestamp: number | null = null) {
    this.urlHash.setHash(SongSource.DIRECT, null, null, compressedData, timestamp);
    this.loadFileFromCompressedData(compressedData);
  }



  async handleHashChange() {
    await this.urlHash.updateState(location.hash);
  }



  async init() {
    await ChartList.init();
    R.i.init();
    Keyboard.init();
    Mouse.init();
    Touch.init();
    Toolbar.init();
    MusicManager.init();

    let that = this;

    // Handle events
    window.addEventListener("hashchange", async function () {
      await that.handleHashChange();
    });
    await this.handleHashChange();

    window.addEventListener("resize", function () {
      R.i.updateSize();
    });
    R.i.updateSize();

    setInterval(function () {
      that.loop();
    }, 10);
    setInterval(function () {
      that.countFps();
    }, 1000);

    window.requestAnimationFrame(function (time) {
      R.i.draw(time);
    });
  }

  loop() {
    let now = Date.now();
    let deltaTime = now - this.lastLoop;

    MusicManager.update();

    if (MusicManager.audio.paused) {
      if (
        Keyboard.isKeyPressed("ArrowDown") &&
        !(Keyboard.control || Keyboard.shift)
      ) {
        this.state.currentTick -= Math.floor(
          (1 / this.state.zoom) * Settings.scrollSpeed * deltaTime
        );
        R.i.setDrawPlayfield();
      }
      if (
        Keyboard.isKeyPressed("ArrowUp") &&
        !(Keyboard.control || Keyboard.shift)
      ) {
        this.state.currentTick += Math.floor(
          (1 / this.state.zoom) * Settings.scrollSpeed * deltaTime
        );
        R.i.setDrawPlayfield();
      }
    } else {
      if (Keyboard.isKeyPressed("ArrowDown")) {
        MusicManager.setAudioSpeed(Keyboard.shift ? 0.25 : 0.5);
      } else if (Keyboard.isKeyPressed("ArrowUp")) {
        MusicManager.setAudioSpeed(Keyboard.shift ? 4 : 2);
      } else {
        MusicManager.setAudioSpeed(1);
      }
    }

    if (this.state.selectedTool !== null) {
      this.state.selectedTool.loop();
    }

    if (this.currentChart !== null) {
      if (MusicManager.audio.paused) {
        MusicManager.seek(this.currentChart.tickToMs(this.state.currentTick));
      } else {
        this.state.currentTick = Math.floor(
          this.currentChart.msToTick(MusicManager.getTime())
        );
        R.i.setDrawPlayfield();
      }
    }

    this.tick++;
    this.lastLoop = now;
  }

  static #i: App = null;
  static get i() {
    if (this.#i === null) {
      this.#i = new App();
    }
    return this.#i;
  }
}

let state = App.i.state;
let style = App.i.style;

export default App;
export { state, style };
