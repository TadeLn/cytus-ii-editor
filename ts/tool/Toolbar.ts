import App from "../App.js";
import SelectTool from "./SelectTool.js";

export default class Toolbar {

  static init() {
    const app = App.i;
    const toolbar = document.getElementById("toolbar");

    const buttons = {
      "select": () => app.changeTool(new SelectTool())
    };

    for (let i = 0; i < 10; i++) {
      buttons[`note-${i}`] = () => app.selectNoteType(i);
    }

    for (const key in buttons) {
      toolbar.querySelector(`#toolbar-button-${key}`).addEventListener("click", buttons[key])
    }
  }

};
