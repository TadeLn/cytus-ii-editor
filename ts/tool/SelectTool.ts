import Tool from "./Tool.js";

class SelectTool extends Tool {
  override getName() {
    return `Select`;
  }

  override render() {}

  override handleMouseDown(x: number, y: number, button: number) {
    return false;
  }
}

export default SelectTool;
