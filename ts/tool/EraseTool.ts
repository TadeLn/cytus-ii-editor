import App, { state, style } from "../App.js";
import Mouse from "../input/Mouse.js";
import Tool from "./Tool.js";
import { default as R } from "../renderer/Renderer.js"
import AABB from "../util/AABB.js"
import Vec2 from "../util/Vec2.js";

class EraseTool extends Tool {
  update() {
    if (Mouse.isButtonPressed(0)) {
      const radius = style.noteRadius;
      const width = radius * 2;
      const chart = App.i.currentChart;

      for (let i = 0; i < chart.noteCount; i++) {
        const note = chart.getNote(i);

        if (state.gameplayMode) {
          // Discard notes that are not on the current page
          if (note.pageIndex !== App.i.currentChart.getPageFromTick(state.currentTick)) {
            continue;
          }
        }

        const [centerX, centerY] = App.i.currentChart.getNotePosition(note, state.currentPage);

        let aabb = new AABB(centerX - radius, centerY - radius, width, width);
        if (aabb.intersectsPoint(new Vec2(Mouse.x, Mouse.y))) {
          App.i.currentChart.removeNote(note.id);
        }
      }
    }
  }

  override getName() {
    return `Erase`;
  }

  override render() {}

  override loop() {
    this.update();
  }

  override handleMouseMove(x: number, y: number) {
    this.update();
    return false;
  }

  override handleMouseDown(x: number, y: number, button: number) {
    this.update();
    return false;
  }
}

export default EraseTool;
