import Renderer from "../renderer/Renderer";

abstract class Tool {
  abstract getName(): string;

  render(r: Renderer) {}

  loop() {}

  handleKeyDown(key: string): boolean {
    return false;
  }

  handleKeyUp(key: string): boolean {
    return false;
  }

  handleMouseMove(x: number, y: number): boolean {
    return false;
  }

  handleMouseDown(x: number, y: number, button: number): boolean {
    return false;
  }

  handleMouseUp(x: number, y: number, button: number): boolean {
    return false;
  }

  handleMouseWheel(deltaY: number): boolean {
    return false;
  }

  destruct() {}
}

export default Tool;
