import App, { state, style } from "../App.js";
import Mouse from "../input/Mouse.js";
import Tool from "./Tool.js";
import { default as R } from "../renderer/Renderer.js"
import AABB from "../util/AABB.js"
import Vec2 from "../util/Vec2.js";

export default class LinkTool extends Tool {
  #isLinking = false;
  #linkedANoteAlready = false;
  #currentlyLinkingNoteId = 0;

  get isLinking() {
    return this.#isLinking;
  }

  override getName() {
    return `Link`;
  }

  override render() {
    if (this.#isLinking) {
      const chart = App.i.currentChart;
      const note = chart.getNote(this.#currentlyLinkingNoteId);

      if (note !== null) {
        const notePosition = chart.getNotePosition(note, chart.getPage(note.truePageIndex));
        const ctx = R.i.ctx;

        ctx.lineWidth = 5;
        ctx.strokeStyle = "#fff";
        ctx.beginPath();
        ctx.moveTo(notePosition[0], notePosition[1]);
        ctx.lineTo(Mouse.x, Mouse.y);
        ctx.stroke();
      }
    }
  }

  override handleMouseMove(x: number, y: number) {
    R.i.setDrawPlayfield();
    return false;
  }

  override handleMouseDown(x: number, y: number, button: number) {
    if (button === 0) {
      const radius = style.noteRadius;
      const width = radius * 2;
      const chart = App.i.currentChart;

      for (let i = 0; i < chart.noteCount; i++) {
        const note = chart.getNote(i);

        if (state.gameplayMode) {
          // Discard notes that are not on the current page
          if (note.pageIndex !== App.i.currentChart.getPageFromTick(state.currentTick)) {
            continue;
          }
        }

        const [centerX, centerY] = App.i.currentChart.getNotePosition(note, state.currentPage);

        let aabb = new AABB(centerX - radius, centerY - radius, width, width);
        if (aabb.intersectsPoint(new Vec2(Mouse.x, Mouse.y))) {

          R.i.setDrawPlayfield();

          this.#linkedANoteAlready = false;
          
          if (this.#isLinking) {
            const lastNote = chart.getNoteById(this.#currentlyLinkingNoteId);
            lastNote.nextId = note.id;
            this.#linkedANoteAlready = true;
          }

          this.#isLinking = true;
          this.#currentlyLinkingNoteId = note.id;
          break;
        }
      }
      return true;

    } else if (button === 2) {
      // Cancel current link

      if (!this.#linkedANoteAlready) {
        const chart = App.i.currentChart;
        const note = chart.getNoteById(this.#currentlyLinkingNoteId);
        note.nextId = -1;
      }

      this.#isLinking = false;
      this.#linkedANoteAlready = false;

      R.i.setDrawPlayfield();
      return true;
    }
    
    return false;
  }
}
