import App, { state } from "../App.js";
import Chart from "../chart/Chart.js";
import Note from "../chart/Note.js";
import NoteType, { getNoteName, isDrag, isHold } from "../chart/NoteType.js";
import Keyboard from "../input/Keyboard.js";
import Mouse from "../input/Mouse.js";
import { default as R } from "../renderer/Renderer.js";
import Settings from "../Settings.js";
import Tool from "./Tool.js";

class NoteTool extends Tool {
  note: Note;

  changeNoteType(type: NoteType) {
    this.note.type = type;
    R.i.setDrawPlayfield();
  }

  override getName() {
    return `Note Tool | ${getNoteName(this.note.type)}`;
  }

  override render() {
    let chart = App.i.currentChart;
    let [noteX, tick] = [
      Chart.snapXToGrid(Chart.getNoteXFromX(Mouse.x)),
      chart.snapTickToGrid(chart.getTickFromY(Mouse.y, state.currentPage)),
    ];
    this.note.isForward = Keyboard.shift;
    this.note.tick = tick;
    this.note.x = noteX;
    const pageIndex = state.gameplayMode
      ? chart.getPageFromTick(state.currentTick)
      : chart.getPageFromTick(tick);
    let [x, y] = chart.getNotePosition(this.note, chart.getPage(pageIndex));
    this.note.pageIndex = pageIndex;
    this.note.draw(x, y, pageIndex, 1, false, true);
  }

  override handleKeyDown(key: string): boolean {
    if (key === "ShiftLeft" || key === "ShiftRight") {
      R.i.setDrawPlayfield();
    }
    return false;
  }

  override handleKeyUp(key: string): boolean {
    if (key === "ShiftLeft" || key === "ShiftRight") {
      R.i.setDrawPlayfield();
    }
    return false;
  }

  override handleMouseDown(x: number, y: number, button: number) {
    if (button === 0) {
      App.i.currentChart.addNote(this.note);
      if (this.note.type === NoteType.DRAG_NOTE) {
        this.changeNoteType(NoteType.DRAG_TICK);
      }
      if (this.note.type === NoteType.HIT_DRAG_NOTE) {
        this.changeNoteType(NoteType.HIT_DRAG_TICK);
      }
      
    } else if (button === 2) {
      console.log("Test")
    }
    return false;
  }

  override handleMouseWheel(deltaY: number) {
    if (isHold(this.note.type)) {
      let amount = App.i.currentChart.timeBase / Settings.gridY;
      if (Keyboard.shift) {
        amount = 1;
      }

      if (deltaY < -1) {
        this.note.duration += amount;
      } else if (deltaY > 1) {
        this.note.duration -= amount;
      }
      if (this.note.duration < 0) {
        this.note.duration = 0;
      }
      this.note.duration = Math.floor(this.note.duration / amount) * amount;

      R.i.setDrawPlayfield();
      return true;
    }
    return false;
  }

  constructor() {
    super();
    this.note = new Note();
  }
}

export default NoteTool;
