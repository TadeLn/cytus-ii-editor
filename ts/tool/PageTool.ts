import App, { state, style } from "../App.js";
import Tool from "./Tool.js";
import Keyboard from "../input/Keyboard.js";

class PageTool extends Tool {
  override getName() {
    return `Page Tool`;
  }

  override handleKeyDown(key: string): boolean {
    if (key === "KeyA") {
      let number = undefined;
      if (Keyboard.shift) {
        let chart = App.i.currentChart;
        let lastPage = chart.getPage(chart.pageCount - 1);
        number = parseInt(prompt("Ticks:", `${lastPage.endTick - lastPage.startTick}`));
      }
      App.i.currentChart.pushPage(number);
      return true;
    }
    if (key === "KeyS") {
      App.i.currentChart.popPage();
      return true;
    }
    return false;
  }
}

export default PageTool;
