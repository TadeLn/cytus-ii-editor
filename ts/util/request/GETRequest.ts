import HTTPRequest from "./HTTPRequest.js";

class GETRequest extends HTTPRequest {

  constructor(url: string) {
    super("GET", url, null);
  }

};

export default GETRequest;
