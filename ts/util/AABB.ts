import Vec2 from "./Vec2.js";

class AABB {
  x: number;
  y: number;
  w: number;
  h: number;

  intersectsBox(box: AABB) {
    return (box.x + box.w > this.x && box.x < this.x + this.w && box.y + box.h > this.y && box.y < this.y + this.h);
  }

  intersectsPoint(point: Vec2) {
    return (point.x >= this.x && point.x < this.x + this.w && point.y >= this.y && point.y < this.y + this.h);
  }

  constructor(x: number, y: number, w: number, h: number) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
};

export default AABB;
