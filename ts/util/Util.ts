namespace Util {
  export function getTimeString(ms: number) {
    let milliseconds = Math.floor(ms % 1000);
    let millisecondsStr = "" + milliseconds;
    if (milliseconds < 100) {
      if (milliseconds < 10) {
        millisecondsStr = "0" + milliseconds;
      }
      millisecondsStr = "0" + milliseconds;
    }

    let seconds = Math.floor((ms / 1000) % 60);
    let secondsStr = "" + seconds;
    if (seconds < 10) {
      secondsStr = "0" + seconds;
    }

    let minutes = Math.floor(ms / 1000 / 60);
    return `${minutes}:${secondsStr}.${millisecondsStr}`;
  }

  export function limitRange(min: number, max: number, value: number) {
    return Math.max(min, Math.min(max, value));
  }

  export function assertType(type: string, value: any) {
    if (typeof value === type) {
      return value;
    } else {
      throw new Error(`Assertion failed: ${value} is not of type ${type}`);
    }
  }

  export function assertArray(value: any) {
    if (Array.isArray(value)) {
      return value;
    } else {
      throw new Error(`Assertion failed: ${value} is not an array`);
    }
  }

  export function assertTypeOrDefault(type: string, value: any, defaultValue: any) {
    if (typeof value === type) {
      return value;
    } else {
      return defaultValue;
    }
  }

  export function assertArrayOrDefault(value: any, defaultValue: any) {
    if (Array.isArray(value)) {
      return value;
    } else {
      return defaultValue;
    }
  }



  export function lzwCompress(source: string, values = 256, tableEntries = 4096): number[] {
    let out = [];

    let dict = {};
    let newTermCode = values;

    let phrase = source.charAt(0);

    for (let i = 1; i < source.length; i++) {
      const currentChar = source.charAt(i);
      const newPhrase = phrase + currentChar;

      // If the new phrase already exists in dictionary, continue reading characters
      if (dict[newPhrase] !== undefined) {
        phrase = newPhrase;

      // If the new phrase doesn't exist in dictionary, add the current phrase to the dictionary, and to the output
      } else {
        if (phrase.length > 1) {
          // If phrase is longer than 1 character, add dict code to output
          out.push(dict[phrase]);

        } else {
          // If phrase is 1 character, add the character code itself
          out.push(phrase.charCodeAt(0)); // [TODO] fairly sure this breaks if code is greater than 255
        }

        // If there is still space, add a new dict entry
        if (newTermCode < tableEntries) {
          dict[newTermCode] = newPhrase;
          newTermCode++;
        }

        // Start a new phrase from here
        phrase = currentChar;
      }
    }

    // Add the last phrase
    if (phrase.length > 1) {
      out.push(dict[phrase]);
    } else {
      out.push(phrase.charCodeAt(0)); // [TODO] fairly sure this breaks if code is greater than 255
    }

    console.warn("lzwCompress", source, out);
    return out;
  }

  export function lzwDecompress(source: number[], values = 256, tableEntries = 4096): string {
    let currentChar = String.fromCharCode(source[0]);
    let out = currentChar;
    
    let dict = {};
    let newTermCode = values;

    let phrase = "";
    let oldPhrase = currentChar;

    for (let i = 1; i < source.length; i++) {
      const currentCode = source[i];
      const newPhrase = oldPhrase + currentChar;

      // Set current phrase
      if (currentCode < values) {
        // Normal ASCII code
        phrase = String.fromCharCode(source[i]);

      } else {
        // Special code, read from dictionary

        if (currentCode in dict) {
          phrase = dict[currentChar];

        } else {
          // If the code doesn't exist in the dictionary, use newPhrase
          phrase = newPhrase;

        }
      }

      // Output phrase
      out += phrase;

      // Set currentChar to first character in phrase
      currentChar = phrase.charAt(0);


      // If there is still space, add a new dict entry
      if (newTermCode < tableEntries) {
        dict[newTermCode] = newPhrase;
        newTermCode++;
      }

      oldPhrase = phrase;
    }

    return out;
  }



  const BASE_64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".split("");

  export function lzwCompress64(source: string, littleEndian = false): string {
    const compressedCodes = lzwCompress(source, 256, 4096);
    let result = "";

    compressedCodes.forEach(x => {
      // x should never be above 64 * 64 = 4096
      const mostSignificantDigit = Math.floor(x / 64);
      const leastSignificantDigit = x % 64;
      if (littleEndian) {
        result += BASE_64[leastSignificantDigit] + BASE_64[mostSignificantDigit];
      } else {
        result += BASE_64[mostSignificantDigit] + BASE_64[leastSignificantDigit];
      }
    });

    return result;
  }

  export function lzwDecompress64(source: string, littleEndian = false): string {
    let compressedCodes = [];
    for (let i = 0; i < source.length; i += 2) {
      const mostSignificantDigit  = BASE_64.findIndex(x => x == source.charAt(littleEndian ? i + 1 : i));
      const leastSignificantDigit = BASE_64.findIndex(x => x == source.charAt(littleEndian ? i     : i + 1));
      compressedCodes.push(mostSignificantDigit * 64 + leastSignificantDigit);
    }

    const result = lzwDecompress(compressedCodes, 256, 4096);
    return result;
  }
}

export default Util;
