import App from "../App.js";

enum SongSource {
  SERVER = "s", // Song data is on the server
  LOCAL_STORAGE = "l", // Song data is on the client's device, in the local storage
  DIRECT = "d" // Song data is embedded in the URL
}

class URLHash {
  #source: SongSource | null;
  #filename: string | null;
  #difficulty: number | null;
  #timestamp: number | null;
  #compressedSongData: string | null;

  // Changes state based on given hash string
  async updateState(hash: string) {
    let app = App.i;
    let hashSegments = hash.split("#");

    this.#source = SongSource.SERVER;
    this.#filename = "";
    this.#difficulty = 0;
    this.#compressedSongData = "";
    this.#timestamp = null;

    if (hashSegments.length > 1) {
      const source = hashSegments[1];
      if (Object.values(SongSource).includes(<SongSource> source)) {
        this.#source = <SongSource>(source);
      }
    }

    if (this.#source === SongSource.SERVER || this.#source === SongSource.LOCAL_STORAGE) {
      // Server side or local storage source
      if (hashSegments.length > 2) {
        const filename = hashSegments[2];
        this.#filename = filename;
      }

      if (hashSegments.length > 3) {
        const difficulty = parseInt(hashSegments[3]);
        if (!isNaN(difficulty)) {
          this.#difficulty = difficulty;
        }
      }

      if (hashSegments.length > 4) {
        const timestamp = parseInt(hashSegments[4]);
        if (!isNaN(timestamp)) {
          this.#timestamp = timestamp;
        }
      }

    } else if (this.#source === SongSource.DIRECT) {
      // From URL
      if (hashSegments.length > 2) {
        const compressedSongData = hashSegments[2];
        this.#compressedSongData = compressedSongData;
      }

      if (hashSegments.length > 3) {
        const timestamp = parseInt(hashSegments[3]);
        if (!isNaN(timestamp)) {
          this.#timestamp = timestamp;
        }
      }
    }

    if (this.#source === SongSource.SERVER) {
      await app.loadFileFromServer(this.#filename, this.#difficulty);
    } else if (this.#source === SongSource.DIRECT) {
      app.loadFileFromCompressedData(this.#compressedSongData);
    }

    if (this.#timestamp !== null) {
      app.state.currentTick = this.#timestamp;
    }
    
  }

  // Changes location.hash based on state of URLHash
  updateHash() {
    if (this.#source === SongSource.DIRECT) {
      this.setHashFromArgs(
        this.#source,
        this.#compressedSongData,
        this.#timestamp
      )
    } else {
      this.setHashFromArgs(
        this.#source !== null ? this.#source : SongSource.SERVER,
        this.#filename !== null ? this.#filename : "",
        this.#difficulty !== null ? this.#difficulty : "0",
        this.#timestamp
      )
    }
  }

  // Changes location.hash based on args
  setHash(source: SongSource, filename: string | null, difficulty: number | null, compressedSongData: string | null, timestamp: number | null) {
    this.#source = source;
    this.#filename = filename;
    this.#difficulty = difficulty;
    this.#compressedSongData = compressedSongData;
    this.#timestamp = timestamp;
    this.updateHash();
  }

  private setHashFromArgs(...args: (number|string)[]) {
    location.hash = args.join("#");
  }
}

export default URLHash;
export { SongSource };
