import { default as R } from "../renderer/Renderer.js";

class Touch {
  static touches = [];

  static init() {
    let cnv = R.i.canvas;
    cnv.addEventListener("touchstart", function (e) {
      if (Touch.handleTouchStart(e)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("touchmove", function (e) {
      if (Touch.handleTouchMove(e)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("touchend", function (e) {
      if (Touch.handleTouchEnd(e)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("touchcancel", function (e) {
      if (Touch.handleTouchCancel(e)) {
        e.preventDefault();
      }
    });
  }



  static handleTouchStart(e: TouchEvent) {
    console.log(e);
    R.i.setDrawPlayfield();
    return true;
  }

  static handleTouchMove(e: TouchEvent) {
    console.log(e);
    R.i.setDrawPlayfield();
    return true;
  }
  
  static handleTouchEnd(e: TouchEvent) {
    console.log(e);
    R.i.setDrawPlayfield();
    return true;
  }
  
  static handleTouchCancel(e: TouchEvent) {
    console.log(e);
    R.i.setDrawPlayfield();
    return true;
  }
}

export default Touch;
