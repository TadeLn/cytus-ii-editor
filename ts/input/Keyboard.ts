import App, { state } from "../App.js";
import NoteType from "../chart/NoteType.js";
import MusicManager from "../music/MusicManager.js";
import { default as R } from "../renderer/Renderer.js";
import HelpScreen from "../screen/HelpScreen.js";
import SongSelectScreen from "../screen/SongSelectScreen.js";
import Settings from "../Settings.js";
import EraseTool from "../tool/EraseTool.js";
import LinkTool from "../tool/LinkTool.js";
import PageTool from "../tool/PageTool.js";
import SelectTool from "../tool/SelectTool.js";
import Util from "../util/Util.js";

class Keyboard {
  private static isInitialized = false;
  private static keys = {};

  static init() {
    if (this.isInitialized) return;

    window.addEventListener("keydown", async (e) => {
      this.keys[e.code] = true;
      if (await Keyboard.handleKeyDown(e.code)) {
        e.preventDefault();
      }
    });

    window.addEventListener("keyup", (e) => {
      this.keys[e.code] = false;
      if (Keyboard.handleKeyUp(e.code)) {
        e.preventDefault();
      }
    });

    window.addEventListener("blur", () => {
      // Unpress all keys
      for (const key of Object.keys(this.keys)) {
        if (this.keys[key]) {
          Keyboard.handleKeyUp(key);
        }
      }
      this.keys = {};
    });

    this.isInitialized = true;
  }

  static isKeyPressed(key: string): boolean {
    return this.keys[key] === undefined ? false : this.keys[key];
  }

  static getPressedKeys() {
    return Object.keys(this.keys).filter(x => this.isKeyPressed(x));
  }

  static get shift() {
    return this.isKeyPressed("ShiftLeft") || this.isKeyPressed("ShiftRight");
  }

  static get control() {
    return this.isKeyPressed("ControlLeft") || this.isKeyPressed("ControlRight");
  }

  static get alt() {
    return this.isKeyPressed("AltLeft") || this.isKeyPressed("AltRight");
  }



  static async handleKeyDown(code: string) {
    let app = App.i;
    if (Settings.showInput || Settings.showModifierKeys) {
      R.i.setDrawPlayfield();
    }



    // Global shortcuts
    // Save
    if (code === "KeyS") {
      if (Keyboard.control) {
        let data = JSON.stringify(App.i.currentChart.toC2());
        localStorage.setItem("backupSave", data);
        navigator.clipboard.writeText(data);
        console.log("Saved level to clipboard");
        return true;
      }
    }

    // Save to URL
    if (code == "KeyQ") {
      if (Keyboard.control) {
        const chart = App.i.currentChart;
        const c2data = chart.toC2Min();
        const data = {
          filename: chart.filename,
          difficulty: chart.difficulty,
          chartData: c2data,
          music: MusicManager.source
        }
        
        const json = JSON.stringify(data);
        const compressedData = Util.lzwCompress64(json);
        console.log(data, json, compressedData);
        App.i.changeFileFromCompressedData(compressedData);

        return true;
      }
    }



    // Save timestamp to URL
    if (code === "KeyO") {
      app.urlHash.setHash(app.currentChart.source, app.currentChart.filename, app.currentChart.difficulty, null, app.state.currentTick);
      navigator.clipboard.writeText(location.href);
      return true;
    }

    if (App.i.currentScreen !== null) {
      if (App.i.currentScreen.onKeyDown(code)) {
        return true;
      }

    } else {
      // Main editor screen key shortcuts
      // Tool keys
      if (code === "Escape") {
        app.changeTool(null);
        return true;
      }
      if (app.state.selectedTool !== null) {
        if (app.state.selectedTool.handleKeyDown(code)) {
          return true;
        }
      }

      // Change tools
      if (code === "Digit1") {
        app.selectNoteType(NoteType.HIT_NOTE);
        return true;
      } else if (code === "Digit2") {
        app.selectNoteType(NoteType.HOLD_NOTE);
        return true;
      } else if (code === "Digit3") {
        app.selectNoteType(NoteType.HOLD_NOTE_LONG);
        return true;
      } else if (code === "Digit4") {
        app.selectNoteType(NoteType.DRAG_NOTE);
        return true;
      } else if (code === "Digit5") {
        app.selectNoteType(NoteType.DRAG_TICK);
        return true;
      } else if (code === "Digit6") {
        app.selectNoteType(NoteType.SWIPE_NOTE);
        return true;
      } else if (code === "Digit7") {
        app.selectNoteType(NoteType.HIT_DRAG_NOTE);
        return true;
      } else if (code === "Digit8") {
        app.selectNoteType(NoteType.HIT_DRAG_TICK);
        return true;
      } else if (code === "Digit9") {
        app.selectNoteType(NoteType.PIANO_NOTE);
        return true;
      } else if (code === "Digit0") {
        app.selectNoteType(NoteType.PIANO_DRAG_NOTE);
        return true;
      } else if (code === "Backquote") {
        app.changeTool(new SelectTool());
        return true;
      } else if (code === "KeyK") {
        app.changeTool(new EraseTool());
        return true;
      } else if (code === "KeyY") {
        app.changeTool(new PageTool());
        return true;
      } else if (code === "KeyJ") {
        app.changeTool(new LinkTool());
        return true;
      } else if (code === "keyI") {
        // [TODO] add tempo and event tools
        // app.changeTool(new TempoTool());
        return true;
      } else if (code === "keyL") {
        // app.changeTool(new EventTool());
        return true;
      }



      // Move through time
      if (app.currentChart !== null) {
        if (code === "ArrowUp") {
          if (Keyboard.control) {
            if (Keyboard.shift) {
              app.nextStep(app.currentChart.timeBase / Settings.gridY);
            } else {
              app.nextStep();
            }
            R.i.setDrawPlayfield();
            return true;
          } else {
            if (Keyboard.shift) {
              if (MusicManager.audio.paused) {
                app.setTick(app.state.currentTick + 1);
                R.i.setDrawPlayfield();
                return true;
              }
            }
          }
        } else if (code === "ArrowDown") {
          if (Keyboard.control) {
            if (Keyboard.shift) {
              app.previousStep(app.currentChart.timeBase / Settings.gridY);
            } else {
              app.previousStep();
            }
            R.i.setDrawPlayfield();
            return true;
          } else {
            if (Keyboard.shift) {
              if (MusicManager.audio.paused) {
                app.setTick(app.state.currentTick - 1);
                R.i.setDrawPlayfield();
                return true;
              }
            }
          }
        }

        if (code === "Space") {
          MusicManager.toggleAudio();
          return true;
        } else if (code === "Minus") {
          app.zoomOut();
          R.i.setDrawPlayfield();
          return true;
        } else if (code === "Equal") {
          app.zoomIn();
          R.i.setDrawPlayfield();
          return true;
        } else if (code === "Home") {
          app.setTick(0);
          R.i.setDrawPlayfield();
          return true;
        } else if (code === "End") {
          app.setTick(app.currentChart.getNote(app.currentChart.noteCount - 1).tick);
          R.i.setDrawPlayfield();
          return true;
        } else if (code === "PageUp") {
          let pageIndex = app.currentChart.getPageFromTick(
            app.state.currentTick
          );
          if (pageIndex === null) {
            let firstPage = app.currentChart.getPage(0);
            if (app.state.currentTick < firstPage.startTick) {
              app.setTick(firstPage.startTick);
            }
          } else {
            let thisPage = app.currentChart.getPage(pageIndex);
            if (thisPage !== null) {
              app.setTick(thisPage.endTick);
            }
          }
          R.i.setDrawPlayfield();
          return true;
        } else if (code === "PageDown") {
          // [TODO] Separate to different functions
          // [TODO] Store current pageIndex / optimize getPageFromTick(S.currentTick)
          let pageIndex = app.currentChart.getPageFromTick(
            app.state.currentTick
          );
          if (pageIndex === null) {
            let lastPage =
              app.currentChart.getPage(app.currentChart.pageCount - 1);
            if (app.state.currentTick === lastPage.endTick) {
              app.setTick(lastPage.startTick);
            } else if (app.state.currentTick > lastPage.endTick) {
              app.setTick(lastPage.endTick);
            }
          } else {
            let previousPage = app.currentChart.getPage(pageIndex - 1);
            if (previousPage !== null) {
              if (app.state.currentTick === previousPage.endTick) {
                app.setTick(previousPage.startTick);
              } else {
                app.setTick(previousPage.endTick);
              }
            }
          }
          R.i.setDrawPlayfield();
          return true;
        }


        // Grid change
        if (code === "BracketLeft") {
          if (this.shift) {
            Settings.gridX--;
          } else {
            Settings.gridY--;
          }

          if (Keyboard.isKeyPressed("BracketRight")) {
            if (this.shift) {
              Settings.gridX = Settings.defaultGridX;
            } else {
              Settings.gridY = Settings.defaultGridY;
            }
          }
          return true;

        } else if (code === "BracketRight") {
          if (this.shift) {
            Settings.gridX++;
          } else {
            Settings.gridY++;
          }

          if (Keyboard.isKeyPressed("BracketLeft")) {
            if (this.shift) {
              Settings.gridX = Settings.defaultGridX;
            } else {
              Settings.gridY = Settings.defaultGridY;
            }
          }
          return true;
        }
      }


      // Toggles
      if (code === "F3") {
        Settings.extraInfo = !Settings.extraInfo;
        R.i.setDrawPlayfield();
        return true;
      } else if (code === "F5") {
        Settings.groupedMode = !Settings.groupedMode;
        R.i.setDrawPlayfield();
        return true;
      } else if (code === "F6") {
        Settings.showGrid = !Settings.showGrid;
        R.i.setDrawPlayfield();
        return true;
      } else if (code === "F8") {
        Settings.showEvents = !Settings.showEvents;
        R.i.setDrawPlayfield();
        return true;
      }

      // Toggle gameplay mode
      if (code === "Tab") {
        app.state.gameplayMode = !app.state.gameplayMode;
        return true;
      }

      // Other screens
      if (code === "F1") {
        app.screenManager.push(new HelpScreen());
        return true;
      } else if (code === "KeyP") {
        app.screenManager.push(new SongSelectScreen());
        return true;
      }

      // New file
      if (code === "KeyM") {
        if (Keyboard.control) {
          await app.changeFileFromServer();
          return true;
        }
      }

    }

    // Change difficulty
    /*
    else if (key === "F1") {
      app.changeDiff(0);
      return true;

    } else if (key === "F2") {
      app.changeDiff(1);
      return true;

    } else if (key === "F3") {
      app.changeDiff(2);
      return true;

    } else if (key === "F4") {
      app.changeDiff(3);
      return true;

    } else if (key === "F5") {
      app.changeDiff(4);
      return true;

    } else if (key === "F6") {
      app.changeDiff(5);
      return true;
    }
    */

    return false;
  }

  static handleKeyUp(key: string) {
    let app = App.i;
    if (Settings.showInput || Settings.showModifierKeys) {
      R.i.setDrawPlayfield();
    }

    if (key === "ArrowUp" || key === "ArrowDown") {
      if (!MusicManager.audio.paused) {
        MusicManager.audio.playbackRate = 1;
      }
      return true;
    }

    // Tool keys
    if (app.state.selectedTool !== null) {
      if (app.state.selectedTool.handleKeyUp(key)) {
        return true;
      }
    }

    return false;
  }
}

export default Keyboard;
