import App from "../App.js";
import { default as R } from "../renderer/Renderer.js";
import Keyboard from "./Keyboard.js";

class Mouse {
  static x = 0;
  static y = 0;
  static buttons = {};

  static init() {
    let cnv = R.i.canvas;
    
    cnv.addEventListener("mousemove", function (e) {
      Mouse.x = e.offsetX;
      Mouse.y = e.offsetY;
      if (Mouse.handleMouseMove(e.offsetX, e.offsetY)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("mousedown", function (e) {
      Mouse.buttons[e.button] = true;
      if (Mouse.handleMouseDown(e.offsetX, e.offsetY, e.button)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("mouseup", function (e) {
      Mouse.buttons[e.button] = false;
      if (Mouse.handleMouseUp(e.offsetX, e.offsetY, e.button)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("wheel", function (e) {
      if (Mouse.handleMouseWheel(e.deltaY)) {
        e.preventDefault();
      }
    });
    cnv.addEventListener("contextmenu", function (e) {
      e.preventDefault();
    });
  }

  static isButtonPressed(button: number) {
    return this.buttons[button] === undefined ? false : this.buttons[button];
  }



  static handleMouseMove(x: number, y: number) {
    let app = App.i;
    if (app.state.selectedTool !== null) {
      if (app.state.selectedTool.handleMouseMove(x, y)) {
        return true;
      }
    }

    R.i.setDrawPlayfield();
    return false;
  }

  static handleMouseDown(x: number, y: number, button: number) {
    let app = App.i;
    if (app.state.selectedTool !== null) {
      if (app.state.selectedTool.handleMouseDown(x, y, button)) {
        return true;
      }
    }

    return false;
  }

  static handleMouseUp(x: number, y: number, button: number) {
    let app = App.i;
    if (app.state.selectedTool !== null) {
      if (app.state.selectedTool.handleMouseUp(x, y, button)) {
        return true;
      }
    }

    return false;
  }

  static handleMouseWheel(deltaY: number) {
    let app = App.i;
    if (app.state.selectedTool !== null) {
      if (app.state.selectedTool.handleMouseWheel(deltaY)) {
        return true;
      }
    }

    if (app.currentChart !== null) {
      if (Keyboard.shift) {
        if (deltaY < 0) {
          app.zoomIn();
          R.i.setDrawPlayfield();
        } else if (deltaY > 0) {
          app.zoomOut();
          R.i.setDrawPlayfield();
        }
        return true;
      } else {
        if (!Keyboard.control) {
          if (deltaY < 0) {
            app.nextStep();
          } else if (deltaY > 0) {
            app.previousStep();
          }
          return true;
        }
      }
    }
    return false;
  }
}

export default Mouse;
