import App from "./App.js";
import MusicManager from "./music/MusicManager.js";
import Renderer from "./renderer/Renderer.js";
import Settings from "./Settings.js";
import Tool from "./tool/Tool.js";

class State {
  #currentTick = 0;
  #currentPageIndex = null;

  get currentTick() {
    return this.#currentTick;
  }

  set currentTick(tick: number) {
    this.#currentTick = tick;
    if (App.i.currentChart !== null) {
      this.#currentPageIndex = App.i.currentChart.getPageFromTick(this.currentTick);
    } else {
      this.#currentPageIndex = null;
    }
  }

  get currentPageIndex() {
    return this.#currentPageIndex;
  }

  get currentPage() {
    return App.i.currentChart.getPage(this.currentPageIndex);
  }

  #gameplayMode = Settings.defaultGameplayMode;
  get gameplayMode() {
    return this.#gameplayMode;
  }
  set gameplayMode(mode: boolean) {
    this.#gameplayMode = mode;
    if (this.#gameplayMode) {
      this.#storedZoomExponent = this.zoomExponent;
      this.zoomExponent = 0;
    } else {
      this.zoomExponent = this.#storedZoomExponent;
    }
    Renderer.i.setDrawPlayfield();
  }

  get isPlaying() {
    return MusicManager.audio === null ? false : !MusicManager.audio.paused;
  }

  zoomExponent = this.#gameplayMode ? 0 : Settings.defaultZoomExponent;
  get zoom() {
    return Math.pow(1.25, this.zoomExponent);
  }
  #storedZoomExponent = Settings.defaultZoomExponent;

  selectedTool: Tool = null;
}

export default State;
