class Color {
  // From 0 to 255
  #r = 0;
  #g = 0;
  #b = 0;
  #a = 0;

  set r(value) {
    this.#r = Math.min(Math.max(Math.round(value), 0), 255);
  }

  set g(value) {
    this.#g = Math.min(Math.max(Math.round(value), 0), 255);
  }

  set b(value) {
    this.#b = Math.min(Math.max(Math.round(value), 0), 255);
  }

  set a(value) {
    this.#a = Math.min(Math.max(Math.round(value), 0), 255);
  }

  get r() {
    return this.#r;
  }

  get g() {
    return this.#g;
  }

  get b() {
    return this.#b;
  }

  get a() {
    return this.#a;
  }

  set opacity(value) {
    this.a = value * 255;
  }

  toRGBA() {
    return `rgba(${this.#r}, ${this.#g}, ${this.#b}, ${this.#a / 255})`;
  }

  constructor(r: number, g: number, b: number, a = 255) {
    this.r = r;
    this.g = g;
    this.b = b;
    this.a = a;
  }

  static fromFloats(r: number, g: number, b: number, a = 1) {
    return new Color(r * 255, g * 255, b * 255, a * 255);
  }

  copy() {
    return new Color(this.r, this.g, this.b, this.a);
  }

  toString() {
    let rHex = this.r.toString(16);
    if (rHex.length < 2) rHex = "0" + rHex;
    let gHex = this.g.toString(16);
    if (gHex.length < 2) gHex = "0" + gHex;
    let bHex = this.b.toString(16);
    if (bHex.length < 2) bHex = "0" + bHex;

    let aHex = "";
    if (this.a !== 255) {
      aHex = this.a.toString(16);
      if (aHex.length < 2) aHex = 0 + aHex;
    }

    return "#" + rHex + gHex + bHex + aHex;
  }

  // [TODO] handle \r (because some charts have it in the color codes)
  static parseColor(colorString: string) {
    let code = colorString;
    if (colorString[0] === "#") {
      code = colorString.substring(1);
    }
    if (code.length === 3) {
      return new Color(
        parseInt(code[0], 16) * 17,
        parseInt(code[1], 16) * 17,
        parseInt(code[2], 16) * 17,
        255
      );
    } else if (code.length === 4) {
      return new Color(
        parseInt(code[0], 16) * 17,
        parseInt(code[1], 16) * 17,
        parseInt(code[2], 16) * 17,
        parseInt(code[3], 16) * 17
      );
    } else if (code.length === 6) {
      return new Color(
        parseInt(code.substring(0, 2), 16),
        parseInt(code.substring(2, 4), 16),
        parseInt(code.substring(4, 6), 16),
        255
      );
    } else if (code.length === 8) {
      return new Color(
        parseInt(code.substring(0, 2), 16),
        parseInt(code.substring(2, 4), 16),
        parseInt(code.substring(4, 6), 16),
        parseInt(code.substring(6, 8), 16)
      );
    } else {
      throw new Error("Color couldn't be parsed: " + colorString)
    }
  }
}

export default Color;
