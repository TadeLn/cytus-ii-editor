import App, { state, style } from "../App.js";
import Chart from "../chart/Chart.js";
import Mouse from "../input/Mouse.js";
import Color from "./Color.js";
import { getDiffName } from "../chart/Difficulty.js";
import MusicManager from "../music/MusicManager.js";
import Util from "../util/Util.js";
import Settings from "../Settings.js";
import Note from "../chart/Note.js";
import Event, { EventType } from "../chart/event/Event.js";
import Keyboard from "../input/Keyboard.js";
import SpeedChangeEvent from "../chart/event/SpeedChangeEvent.js";
import TextEvent from "../chart/event/TextEvent.js";

class Renderer {
  canvas: HTMLCanvasElement;
  ctx: CanvasRenderingContext2D;

  frame = 0;
  fps = 0;
  lastDraw = Date.now();
  lastDrawTime = 0;

  #doDrawPlayfield = false;
  setDrawPlayfield() {
    this.#doDrawPlayfield = true;
  }

  // [TODO] add type annotations
  previousTextEvent = null;
  currentTextEvent = null;

  filterNotes() {
    let chart = App.i.currentChart;
    let result: Array<{ x: number; y: number; pageIndex: number; note: Note }> =
      [];

    for (let i = 0; i < chart.noteCount; i++) {
      let note = chart.getNoteById(i);

      let [x, y] = chart.getNotePosition(note, null);

      const margin = 100;
      let pageIndex = note.truePageIndex;
      if (state.gameplayMode) {
        if (
          // pageIndex > getPageFromTick(S.currentTick + beatToTick(2)) // More visibility
          pageIndex > state.currentPageIndex + 1 // More accurate to the original
        ) {
          // Skip all notes that are too far in the future
          continue;
        }
        if (note.tick + note.duration < state.currentTick && note.pageIndex !== state.currentPageIndex) {
          if (!Settings.showDragNotes) {
            // Skip all notes that are already completely hit (unless they're drag notes and the setting showDragNotes is on)
            continue;
          }

          if (note.nextId === 0 || note.nextId === -1) {
            // Skip all drag notes that don't have a next note and all non-drag notes
            continue;
          } else {
            if (chart.getNoteById(note.nextId).tick < state.currentTick) {
              // Skip all drag notes that which notes have already been hit
              continue;
            }
          }
        }
      } else {
        if (
          y < -margin ||
          (y > this.canvas.height + margin &&
            note.nextId === 0 &&
            note.duration === 0)
        ) {
          // Skip all notes that are outside the rendered screen
          continue;
        }
      }
      result.push({ x, y, pageIndex, note });
    }

    return result;
  }

  updateSize() {
    let cnv = this.canvas;
    let { width, height } = cnv.getBoundingClientRect();
    cnv.width = width;
    cnv.height = height;
    style.update();
    this.setDrawPlayfield();
  }

  init() {
    this.canvas = document.querySelector("#playfield");
    this.ctx = this.canvas.getContext("2d");
  }

  // Drawing functions
  fillAndStroke(fill = null, stroke = null, line = null) {
    let ctx = this.ctx;
    ctx.fillStyle = fill;
    ctx.strokeStyle = stroke;
    ctx.lineWidth = line;
    if (fill !== null) ctx.fill();
    if (stroke !== null) ctx.stroke();
  }

  drawRect(
    x: number,
    y: number,
    w: number,
    h: number,
    style?: { fill?: string; stroke?: string; line?: string }
  ) {
    let ctx = this.ctx;
    ctx.rect(x, y, w, h);
    this.fillAndStroke(style.fill, style.stroke, style.line);
  }

  drawCircle(
    x: any,
    y: any,
    r: any,
    style?: { fill?: string; stroke?: string; line?: string }
  ) {
    // [TODO]
    let ctx = this.ctx;
    this.fillAndStroke(style.fill, style.stroke, style.line);
  }

  drawTextWithShadow(
    text: string,
    x: number,
    y: number,
    opacity = 1,
    maxWidth?: number
  ) {
    let ctx = this.ctx;
    const shadowOffset = 3;

    let style = ctx.fillStyle;
    ctx.fillStyle = `rgba(64, 64, 64, ${opacity * 0.5})`;
    ctx.fillText(text, x + shadowOffset, y + shadowOffset, maxWidth);
    ctx.fillStyle = style;
    ctx.fillText(text, x, y, maxWidth);
  }

  drawEventLine(
    y: number,
    color: string | CanvasGradient | CanvasPattern,
    lineDash?: number[]
  ) {
    let ctx = this.ctx;
    if (!Settings.showEvents) return;

    const lineWidth = 8;

    if (lineDash !== undefined) {
      ctx.setLineDash(lineDash);
    }

    ctx.strokeStyle = color;
    ctx.lineWidth = lineWidth;
    ctx.beginPath();
    ctx.moveTo(0, y);
    ctx.lineTo(this.canvas.width, y);
    ctx.stroke();

    if (lineDash !== undefined) {
      ctx.setLineDash([]);
    }
  }

  drawEvents(
    y: number,
    color: string,
    right: boolean,
    strings: string | any[]
  ) {
    let ctx = this.ctx;
    if (!Settings.showEvents) return;
    this.drawEventLine(y, color);

    const textMargin = 10;
    const length = strings.length;
    let x: number;

    if (right) {
      x = this.canvas.width - style.eventWidth;
    } else {
      x = 0;
    }

    ctx.fillStyle = "#7f7f7f7f";
    ctx.fillRect(x, y, style.eventWidth, length * 40);

    for (let j = 0; j < length; j++) {
      let string = strings[j];

      ctx.font = "20px Electrolize";
      ctx.fillStyle = "#fff";
      ctx.textAlign = "left";
      ctx.textBaseline = "top";
      this.drawTextWithShadow(
        string,
        x + textMargin,
        y + j * 40 + textMargin,
        1,
        style.eventWidth - textMargin * 2
      );
    }
  }

  drawTextEvent() {
    const chart = App.i.currentChart;
    const ctx = this.ctx;
    if (this.currentTextEvent !== null) {
      const tick = this.currentTextEvent.tick;
      let color = this.currentTextEvent.color.copy();
      const text = this.currentTextEvent.text;

      ctx.font = "bold 30px Electrolize";

      let opacity = 0;
      const opacityFadeIn = 0;
      const opacityHold = 1500;
      const opacityFadeOut = 100;

      if (state.currentTick > tick) {
        opacity = Util.limitRange(0, 1, (chart.tickToMs(state.currentTick) - chart.tickToMs(tick)) / opacityFadeIn);
      }
      if (state.currentTick > tick + opacityFadeIn + opacityHold) {
        opacity = 1 - Util.limitRange(0, 1, (chart.tickToMs(state.currentTick) - (chart.tickToMs(tick) + opacityFadeIn + opacityHold)) / opacityFadeOut)
      }

      let letterSpacing = 0;
      const letterSpacingFadeIn = 0;
      const letterSpacingHold = 1000;
      const letterSpacingFadeOut = 600;

      if (state.currentTick > tick + letterSpacingFadeIn + letterSpacingHold) {
        letterSpacing = Util.limitRange(0, 1, (chart.tickToMs(state.currentTick) - (chart.tickToMs(tick) + letterSpacingFadeIn + letterSpacingHold)) / letterSpacingFadeOut)
      }

      color.a *= opacity;

      ctx.fillStyle = color.toRGBA();
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      (<any>ctx).letterSpacing = `${letterSpacing === 0 ? 0 : Math.pow(2, 10 * letterSpacing - 10) * 100}px`;

      if (Math.floor(letterSpacing * 15) % 2 === 0) {
        this.drawTextWithShadow(
          text,
          this.canvas.width / 2,
          this.canvas.height - 150,
          opacity
        );
      }
      (<any>ctx).letterSpacing = "0px";
    }
  }

  updateScanlineColor() {
    if (this.currentTextEvent !== null) {
      const tick = this.currentTextEvent.tick;
      const color = this.currentTextEvent.color;

      const transition = 200;
      const hold = 2000;

      let previousColor: Color;
      let thisColor = color;

      let progress: number;
      if (state.currentTick - tick < transition) {
        // Fade in
        if (
          this.previousTextEvent !== null &&
          tick < this.previousTextEvent.tick + hold + transition
        ) {
          previousColor = this.previousTextEvent.color;
        } else {
          previousColor = new Color(255, 255, 255);
        }
        progress = Math.max(
          0,
          Math.min(1, (state.currentTick - tick) / transition)
        );
      } else {
        // Fade out
        previousColor = new Color(255, 255, 255);
        progress = 1 - Util.limitRange(0, 1, (state.currentTick - (tick + transition + hold)) / transition);
      }

      style.scanLineColor = new Color(
        thisColor.r * progress + previousColor.r * (1 - progress),
        thisColor.g * progress + previousColor.g * (1 - progress),
        thisColor.b * progress + previousColor.b * (1 - progress),
        thisColor.a * progress + previousColor.a * (1 - progress)
      );
    }
  }

  drawEvent(tick: number, event: Event) {
    // Speedup | Slowdown
    if (event.type === EventType.SPEED_UP || event.type === EventType.SLOW_DOWN) {
      if (
        state.currentTick >= tick &&
        (this.currentTextEvent === null || tick >= this.currentTextEvent.tick)
      ) {
        const textEvent = (<SpeedChangeEvent>event).makeTextEvent();
        this.previousTextEvent = this.currentTextEvent;
        this.currentTextEvent = { tick: tick, color: textEvent.color, text: textEvent.text };
      }
    }

    // Text
    if (event.type === EventType.TEXT) {
      if (
        state.currentTick >= tick &&
        (this.currentTextEvent === null || tick >= this.currentTextEvent.tick)
      ) {
        const textEvent = (<TextEvent>event);
        this.previousTextEvent = this.currentTextEvent;
        this.currentTextEvent = { tick: tick, color: textEvent.color, text: textEvent.text };
      }
    }
  }

  drawPage(startY: number, endY: number, i: number, direction: number) {
    let chart = App.i.currentChart;
    let page = chart.getPage(i);

    let ctx = this.ctx;
    ctx.lineWidth = 10;
    ctx.strokeStyle = "#ffffff7f";
    ctx.fillStyle = "#7f7f7f28";
    ctx.beginPath();
    ctx.rect(
      style.playfieldPadding,
      endY,
      style.playfieldInnerWidth,
      startY - endY
    );
    ctx.fill();
    ctx.stroke();

    if (Settings.showGrid) {
      let pageBeats = (page.endTick - page.startTick) / chart.timeBase;
      let gridLineCount = Settings.gridY * pageBeats;
      for (let i = 0; i <= gridLineCount; i++) {
        let y = i / gridLineCount;

        const fn = page.positionFunction;
        if (fn !== null && App.i.state.gameplayMode) {
          y = (y * 2) - 1;
          y *= fn.pageLength;
          y -= fn.pageCenter;
          y = (y + 1) / 2;
        }
        y *= (startY - endY);
        y += endY;

        // Default gray line
        ctx.strokeStyle = "#80808010";
        ctx.lineWidth = 2;

        // Orange, quarter beat lines
        if (i % (Settings.gridY / 4) === 0) {
          ctx.strokeStyle = "#ff804030";
        }

        // Yellow, half beat lines
        if (i % (Settings.gridY / 2) === 0) {
          ctx.strokeStyle = "#ffff4030";
        }

        // Red, beat lines
        if (i % Settings.gridY === 0) {
          ctx.strokeStyle = "#ff404080";
          ctx.lineWidth = 4;
        }

        ctx.beginPath();
        ctx.moveTo(style.playfieldPadding, y);
        ctx.lineTo(style.playfieldPadding + style.playfieldInnerWidth, y);
        ctx.stroke();
      }

      for (let i = 0; i <= Settings.gridX; i++) {
        let x =
          style.playfieldPadding +
          (i * style.playfieldInnerWidth) / Settings.gridX;

        ctx.strokeStyle = "#80808010";
        ctx.lineWidth = 2;
        if (i % 4 === (Settings.gridX / 2) % 4) {
          ctx.strokeStyle = "#4040ff80";
        }

        // Magenta, middle line
        if (i === Settings.gridX / 2) {
          ctx.strokeStyle = "#ff40ff60";
          ctx.lineWidth = 4;
        }

        ctx.beginPath();
        ctx.moveTo(x, startY);
        ctx.lineTo(x, endY);
        ctx.stroke();
      }
    }

    // Draw page number
    let textFontSize = 100;
    const textX = style.playfieldPadding + textFontSize;
    let textY = endY + textFontSize;
    if (textY + textFontSize > startY) {
      textY = endY;
      textFontSize = 50;
    }
    ctx.strokeStyle = "#ffffff80";
    ctx.fillStyle = "#20202080";
    {

      ctx.font = `${textFontSize}px Electrolize`;
      ctx.lineWidth = 3;
      ctx.textAlign = "left";
      ctx.textBaseline = "top";
      ctx.fillText(`${i}`, textX, textY);
      ctx.strokeText(`${i}`, textX, textY);
    }

    if (page.positionFunction !== null) {
      const smallTextFontSize = 30;
      const smallTextX = textX;
      const smallTextY = textY + textFontSize;
      if (textY - textFontSize / 2 > endY + (startY - endY) / 2) {
        textY = endY + (startY - endY) / 2 + textFontSize / 2;
      }

      ctx.font = `${smallTextFontSize}px Electrolize`;
      ctx.lineWidth = 1;
      ctx.textAlign = "left";
      ctx.textBaseline = "top";
      const smallText = `F${page.positionFunction.type}: ${page.positionFunction.pageLength.toFixed(2)} | ${page.positionFunction.pageCenter.toFixed(2)}`;
      ctx.fillText(smallText, smallTextX, smallTextY);
      ctx.strokeText(smallText, smallTextX, smallTextY);
    }

    // Draw direction arrow
    {
      const arrowSize = textFontSize;
      let arrowX =
        this.canvas.width - style.playfieldPadding - textFontSize - arrowSize;
      let arrowCenterY = textY;

      // Converts nubmers from 0 to 1 into actual coordinates
      function c(x: number, y: number) {
        return {
          x: arrowX + x * arrowSize,
          y: arrowCenterY + direction * (y - 0.5) * arrowSize,
        };
      }

      function m(x: number, y: number) {
        let r = c(x, y);
        ctx.moveTo(r.x, r.y);
      }
      function l(x: number, y: number) {
        let r = c(x, y);
        ctx.lineTo(r.x, r.y);
      }

      ctx.beginPath();
      m(0.4, 1.0);
      l(0.4, 0.3);
      l(0.2, 0.3);
      l(0.5, 0.0);
      l(0.8, 0.3);
      l(0.6, 0.3);
      l(0.6, 1.0);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();
    }
  }

  drawPlayfield() {
    let ctx = this.ctx;
    ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    let playfield = document.querySelector("#playfield");
    playfield.innerHTML = "";

    let chart = App.i.currentChart;
    if (chart !== null) {
      const currentPageIndex = state.currentPageIndex;
      const currentPage = state.currentPage;
      const currentPageLength = state.currentPage === null ? null : state.currentPage.endTick - state.currentPage.startTick;

      // Draw pages
      if (state.gameplayMode) {
        if (currentPage !== null) {
          this.drawPage(
            this.canvas.height - style.verticalMargin,
            style.verticalMargin,
            currentPageIndex,
            currentPage.direction
          );
        }
      } else {
        for (let i = 0; i < chart.pageCount; i++) {
          let page = chart.getPage(i);
          let startY = Chart.getYFromTickHighway(page.startTick);
          let endY = Chart.getYFromTickHighway(page.endTick);

          const margin = 100;
          if (startY < -margin || endY > this.canvas.height + margin) {
            continue;
          }

          this.drawPage(startY, endY, i, page.direction);
        }
      }

      // Draw notes
      let filteredNotes = this.filterNotes();
      for (const { x, y, pageIndex, note } of filteredNotes) {
        if (!state.gameplayMode) {
          note.draw(x, y, pageIndex, 1);
        } else {

          // Calculate note size
          let active = 0;
          const notePage = chart.getPage(pageIndex);
          const prevPage = chart.getPage(pageIndex - 1);

          // How long has the page been on screen
          const notePageProgress = state.currentTick - notePage.startTick;
          const prevPageProgress = prevPage === null ? 0 : state.currentTick - prevPage.startTick;

          if (Settings.groupedMode) {
            // Grouped popup mode
            const transition = 100; // How many ticks the transition lasts
            active += Util.limitRange(0, 1, prevPageProgress / transition) * 0.5;
            active += Util.limitRange(0, 1, notePageProgress / transition) * 0.5;

          } else {
            // Default popup mode
            const notePageLength = notePage.endTick - notePage.startTick;
            const scanlineFraction = currentPageIndex +
              Util.limitRange(0, 1, (state.currentTick - (currentPage !== null ? currentPage.startTick : 0)) / currentPageLength); // A fraction of the current page that has already been completed
            
            const noteFraction = pageIndex + 
              Util.limitRange(0, 1, (note.tick - notePage.startTick) / notePageLength); // A fraction of the current page that has already been completed

            active = Util.limitRange(0, 1, 1 - (noteFraction - scanlineFraction));
          }

          note.draw(x, y, pageIndex, active, undefined, !state.isPlaying);
        }
      }

      // Draw events
      for (let i = 0; i < chart.eventGroupCount; i++) {
        let events = chart.getEventGroup(i);
        for (let event of events.events) {
          this.drawEvent(events.tick, event);
        }
      }

      // Draw event lines
      for (let i = 0; i < chart.eventGroupCount; i++) {
        let events = chart.getEventGroup(i);
        let y = chart.getYFromTick(events.tick, chart.getPage(chart.getPageFromTick(events.tick)));

        const margin = 100;
        if (state.gameplayMode) {
          if (events.tick < state.currentTick) {
            // Ignore events that have been already passed
            continue;
          }
          if (events.tick > state.currentTick + chart.beatToTick(2)) {
            // Ignore events that are more than 2 beats in the future and don't continue
            break;
          }
        } else {
          if (y > this.canvas.height + margin) {
            // Ignore events that are offscreen in the past
            continue;
          }
          if (y < -margin) {
            // Ignore events that are offscreen in the future and don't continue
            break;
          }
        }

        let strings = [];
        for (let event of events.events) {
          strings.push(event.toString());
        }
        this.drawEvents(y, "#00ff007f", true, strings);
      }

      // Draw bpm lines
      for (let i = 0; i < chart.tempoCount; i++) {
        let event = chart.getTempo(i);
        let y = chart.getYFromTick(event.tick, chart.getPage(chart.getPageFromTick(event.tick)));

        const margin = 100;
        if (state.gameplayMode) {
          if (event.tick < state.currentTick) {
            // Ignore events that have been already passed
            continue;
          }
          if (event.tick > state.currentTick + chart.beatToTick(2)) {
            // Ignore events that are more than 2 beats in the future and don't continue
            break;
          }
        } else {
          if (y > this.canvas.height + margin) {
            // Ignore events that are offscreen in the past
            continue;
          }
          if (y < -margin) {
            // Ignore events that are offscreen in the future and don't continue
            break;
          }
        }

        let strings = [
          `${Math.floor(Chart.tempoValueToBpm(event.value) * 100) / 100} bpm`,
          event.value,
        ];
        this.drawEvents(y, "#0000ff7f", false, strings);
      }

      // Update events
      this.drawTextEvent();
      this.updateScanlineColor();
      this.currentTextEvent = null;

      // Draw scanline
      {
        const y = chart.getYFromTick(state.currentTick, state.currentPage);
        // Covered Area
        if (state.gameplayMode) {
          let page = state.currentPage;
          if (page !== null) {
            let color = style.scanLineColor.copy();
            color.a /= 32;
            ctx.fillStyle = color.toRGBA();

            if (page.direction === 1) {
              ctx.fillRect(
                style.playfieldPadding,
                y,
                style.playfieldInnerWidth,
                this.canvas.height - style.verticalMargin - y
              );
            } else {
              ctx.fillRect(
                style.playfieldPadding,
                y,
                style.playfieldInnerWidth,
                style.verticalMargin - y
              );
            }
          }
        }

        // Line
        ctx.fillStyle = style.scanLineColor.toRGBA();
        const lineThickness = 4;
        ctx.fillRect(
          0,
          y - lineThickness / 2,
          this.canvas.width,
          lineThickness
        );
        
      }

      // Draw tool
      if (state.selectedTool !== null) {
        state.selectedTool.render(this);
      }
    }


    if (Settings.showInput) {
      ctx.strokeStyle = "#ff00003f";
      ctx.lineWidth = 1;
      ctx.moveTo(0, Mouse.y);
      ctx.lineTo(this.canvas.width, Mouse.y);
      ctx.moveTo(Mouse.x, 0);
      ctx.lineTo(Mouse.x, this.canvas.height);
      ctx.stroke();
    }

    // Draw text
    let i = 0;
    let that = this;
    function addText(text: string, color = "#fff") {
      const fontSize = 12;
      const lineSize = fontSize + 5;
      const margin = 5;

      ctx.textAlign = "left";
      ctx.textBaseline = "top";
      ctx.font = `${fontSize}px monospace`;
      ctx.fillStyle = color;
      that.drawTextWithShadow(text, margin, margin + lineSize * i++);
    }
    
    if (Settings.extraInfo) {
      addText(`FPS: ${this.fps} TPS: ${App.i.tps}`, "#0f0");
    }

    if (chart != null) {
      addText(
        `Current file: ${chart.filename} [${getDiffName(chart.difficulty)}]`
      );
    }

    addText(
      `[F1] help, [F3] exInfo: ${
        Settings.extraInfo ? "shown" : "hidden"
      }, [F5] popup: ${
        Settings.groupedMode ? "grouped" : "default"
      }, [F6] grid: ${Settings.showGrid ? "shown" : "hidden"}, [F8] events: ${
        Settings.showEvents ? "shown" : "hidden"
      }`,
      "#0ff"
    );
    if (state.selectedTool !== null) {
      addText(`Current tool: ${state.selectedTool.getName()}`, "#ff0");
    }

    const pressedKeys = Keyboard.getPressedKeys();
    if (Settings.extraInfo) {
      ctx.fillStyle = "#fff";

      if (chart !== null) {
        let ms = chart.tickToMs(state.currentTick);
        let endMs = MusicManager.audio.duration * 1000;
        addText(
          `Time: ${Util.getTimeString(ms)}/${Util.getTimeString(
            endMs
          )} [${Math.floor(ms)}ms/${Math.floor(endMs)}ms] (${(
            (ms * 100) /
            endMs
          ).toFixed(1)}%)`
        );
        addText(
          `Page: ${chart.getPageFromTick(state.currentTick)} Tick: ${
            state.currentTick
          } Beat: ${chart.tickToBeat(state.currentTick).toFixed(2)}`
        );
        addText(
          `Zoom level: ${state.zoomExponent} Zoom value: ${state.zoom}`,
          "#fff"
        );
        let currentTempo =
          chart.getTempo(chart.getTempoChangeFromTick(state.currentTick));
        if (currentTempo !== undefined) {
          addText(
            `Tempo: ${Chart.tempoValueToBpm(currentTempo.value).toFixed(
              2
            )}bpm since ${Math.floor(chart.tickToMs(currentTempo.tick))}ms`
          );
          // ${currentTempo.value} @ ${currentTempo.tick}
        }
      }

      if (Settings.showInput) {
        const pressedButtons = Object.keys(Mouse.buttons).filter((button) => {
          return Mouse.buttons[button];
        });
        if (chart !== null) {
          let mouseX = Chart.getNoteXFromX(Mouse.x);
          let mouseTick = chart.getTickFromY(Mouse.y, state.currentPage);
          addText(
            `Mouse: [${Mouse.x}, ${Mouse.y}] => ` +
              `x: ${mouseX.toFixed(3)} [${Chart.snapXToGrid(mouseX)}], ` +
              `tick: ${mouseTick.toFixed(3)} [${chart.snapTickToGrid(
                mouseTick
              )}] Buttons: ${pressedButtons.join(" ")}`
          );
        } else {
          addText(`Mouse: [${Mouse.x}, ${Mouse.y}] Buttons: ${pressedButtons.join(" ")}`);
        }
        addText(`Keyboard: ${pressedKeys.join(" ")}`);
      }
    }

    if (Settings.showModifierKeys) {
      addText(
        pressedKeys
          .filter((a) => {
            return ["Control", "Shift", "Alt"].includes(a);
          })
          .join(" "),
        "#fff"
      );
    }

    /*
    // Draw difficulty select
    {
      const fontSize = 20;
      const rectHeight = 50;
      const rectWidth = 200;

      let diff: number;
      let i = 0;
      ctx.font = `${fontSize}px monospace`;

      diff = ChartList.getDiffByIndex(i);
      while (diff !== undefined) {
        if (App.i.songDiff === diff) {
          ctx.fillStyle = "#00ff007f";
          ctx.fillRect(
            rectWidth * i,
            this.canvas.height - rectHeight,
            rectWidth,
            rectHeight
          );
        }

        ctx.fillStyle = "#fff";
        this.drawTextWithShadow(
          `[F${i + 1}] ${getDiffName(diff, true)}`,
          10 + 200 * i,
          this.canvas.height - rectHeight / 2 + fontSize / 2
        );
        i++;

        diff = ChartList.getDiffByIndex(i);
      }
    }
    */

    this.#doDrawPlayfield = false;
  }

  draw(currentTime: number) {
    let now = Date.now();
    let deltaTime = currentTime - this.lastDrawTime;

    if (this.#doDrawPlayfield) {
      this.drawPlayfield();
    }

    this.lastDraw = now;
    this.lastDrawTime = currentTime;
    this.frame++;
    let that = this;
    window.requestAnimationFrame(function (time) {
      that.draw(time);
    });
  }

  static #i: Renderer = null;
  static get i() {
    if (this.#i === null) {
      this.#i = new Renderer();
    }
    return this.#i;
  }
}

export default Renderer;
