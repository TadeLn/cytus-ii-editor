import Color from "./Color.js";
import { default as R } from "./Renderer.js";

class Style {
  playfieldPadding = 50;

  linePos = 150;
  verticalMargin = 100;
  eventWidth = 300;


  // Overall scale
  noteRadius = 40;

  defaultLineWidth = 0.2;
  dragLineWidth = 0.25;
  pianoNoteLineWidth = 0.2;

  dragArrowRadius = 0.75;
  dragTickRadius = 0.25;
  holdRectWidth = 1;
  pianoNoteWidth = 2.5;
  pianoNoteThickness = 0.5;

  pastNoteColor = new Color(208, 208, 0);
  holdRectColor = new Color(208, 208, 208);
  progressCircleColor = new Color(255, 255, 255);
  dragNoteLine = new Color(242, 191, 255);

  hitNoteFill = new Color(86, 193, 226);
  hitNoteStroke = new Color(51, 138, 165);
  holdNoteFill = new Color(165, 73, 93);
  longHoldNoteFill = new Color(247, 252, 0);
  dragNoteFill = new Color(135, 25, 207);
  dragNoteStroke = new Color(204, 0, 255);
  swipeNoteFill = new Color(49, 206, 172);
  swipeNoteStroke = new Color(208, 208, 208);
  unknownNoteFill = new Color(64, 64, 64);
  unknownNoteStroke = new Color(48, 48, 48);



  // [TODO] move to renderer
  scanLineColor = new Color(255, 255, 255);

  #playfieldInnerWidth = 0;
  get playfieldInnerWidth() {
    return this.#playfieldInnerWidth;
  }

  update() {
    this.#playfieldInnerWidth = R.i.canvas.width - this.playfieldPadding * 2;
  }
}

export default Style;
