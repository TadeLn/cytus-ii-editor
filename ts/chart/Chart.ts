import App, { style, state } from "../App.js";
import { default as R } from "../renderer/Renderer.js";
import Settings from "../Settings.js";
import { SongSource } from "../util/URLHash.js";
import Util from "../util/Util.js";
import EventGroup from "./EventGroup.js";
import Note from "./Note.js";
import NoteType, { isDrag, isPiano } from "./NoteType.js";
import Page from "./Page.js";
import Tempo from "./Tempo.js";

class Chart {
  // ===== FIELDS =====

  #globalOffset = 0; // [TODO] this might not work yet

  // These aren't saved to the file
  #source: SongSource = SongSource.SERVER;
  #filename: string = "";
  #difficulty: number = 0;


  #formatVersion: number;
  #timeBase: number;
  #startOffsetTime: number;
  #pages: Page[];
  #tempos: Tempo[];
  #eventGroups: EventGroup[];
  #notes: Note[];



  // ===== GETERS & SETTERS =====

  get source(): SongSource {
    return this.#source;
  }
  
  get filename(): string {
    return this.#filename;
  }
  
  get difficulty(): number {
    return this.#difficulty;
  }



  get formatVersion(): number {
    return this.#formatVersion;
  }

  get timeBase(): number {
    return this.#timeBase;
  }

  get startOffsetTime(): number {
    return this.#startOffsetTime;
  }

  get pageCount(): number {
    return this.#pages.length;
  }

  getPage(index: number): Page | null {
    let elem = this.#pages[index];
    return elem === undefined ? null : elem;
  }

  hasPage(index: number): boolean {
    return this.#pages[index] !== undefined;
  }

  get tempoCount(): number {
    return this.#tempos.length;
  }

  getTempo(index: number): Tempo | null {
    let elem = this.#tempos[index];
    return elem === undefined ? null : elem;
  }

  get eventGroupCount(): number {
    return this.#eventGroups.length;
  }

  getEventGroup(index: number): EventGroup | null {
    let elem = this.#eventGroups[index];
    return elem === undefined ? null : elem;
  }

  get noteCount(): number {
    return this.#notes.length;;
  }

  getNote(index: number): Note | null {
    let elem = this.#notes[index];
    return elem === undefined ? null : elem;
  }



  // ===== LOAD/SAVE/CONSTRUCT =====

  static fromC2(c2data: any): Chart {
    return new Chart(
      Util.assertType("number", c2data.format_version),
      Util.assertType("number", c2data.time_base),
      Util.assertType("number", c2data.start_offset_time),
      Util.assertArray(c2data.page_list).map((x: any) => Page.fromC2(x)),
      Util.assertArray(c2data.tempo_list).map((x: any) => Tempo.fromC2(x)),
      Util.assertArray(c2data.event_order_list).map((x: any) => EventGroup.fromC2(x)),
      Util.assertArray(c2data.note_list).map((x: any) => Note.fromC2(x))
    );
  }

  toC2() {
    const ver = this.#formatVersion;
    return {
      format_version:    this.#formatVersion,
      time_base:         this.#timeBase,
      start_offset_time: this.#startOffsetTime,
      page_list:         this.#pages.map(x => x.toC2(ver)),
      tempo_list:        this.#tempos.map(x => x.toC2()),
      event_order_list:  this.#eventGroups.map(x => x.toC2()),
      note_list:         this.#notes.map(x => x.toC2()),
    };
  }

  static fromC2Min(c2mindata: any): Chart {
    let noteId = 0;
    let lastPageEnd = 0;
    const chart = new Chart(
      Util.assertTypeOrDefault("number", c2mindata.v, 1),
      Util.assertTypeOrDefault("number", c2mindata.b, 480),
      Util.assertTypeOrDefault("number", c2mindata.o, 0.0),
      Util.assertArrayOrDefault(c2mindata.p, []).map((x: any) => {
        const page = Page.fromC2Min(x, lastPageEnd);
        lastPageEnd = page.endTick;
        return page;
      }),
      Util.assertArrayOrDefault(c2mindata.t, []).map((x: any) => Tempo.fromC2Min(x)),
      Util.assertArrayOrDefault(c2mindata.e, []).map((x: any) => EventGroup.fromC2Min(x)),
      Util.assertArrayOrDefault(c2mindata.n, []).map((x: any) => Note.fromC2Min(x, noteId++))
    );

    for (let i = 0; i < chart.noteCount; i++) {
      const note = chart.getNote(i);
      note.updateHasSibling(chart);
      note.pageIndex = chart.getPageFromTick(note.tick);
    }
    return chart;
  }

  toC2Min() {
    return {
      v: this.#formatVersion      === 1   ? undefined : this.#formatVersion,
      b: this.#timeBase           === 480 ? undefined : this.#timeBase,
      o: this.#startOffsetTime    === 0.0 ? undefined : this.#startOffsetTime,
      p: this.#pages.length       === 0   ? undefined : this.#pages.map(x => x.toC2Min()),
      t: this.#tempos.length      === 0   ? undefined : this.#tempos.map(x => x.toC2Min()),
      e: this.#eventGroups.length === 0   ? undefined : this.#eventGroups.map(x => x.toC2Min()),
      n: this.#notes.length       === 0   ? undefined : this.#notes.map(x => x.toC2Min()),
    };
  }

  static loadFromData(source: SongSource, filename: string, difficulty: number, c2data: any) {
    let result = Chart.fromC2(c2data);
    result.#source = source;
    result.#filename = filename;
    result.#difficulty = difficulty;
    return result;
  }

  constructor(
    formatVersion = 1,  
    timeBase = 480,
    startOffsetTime = 0.0,
    pages: Page[] = [],
    tempos: Tempo[] = [],
    eventGroups: EventGroup[] = [],
    notes: Note[] = []
  ) {
    this.#formatVersion = formatVersion;
    this.#timeBase = timeBase;
    this.#startOffsetTime = startOffsetTime;
    this.#pages = pages;
    this.#tempos = tempos;
    this.#eventGroups = eventGroups;
    this.#notes = notes;
  }



  // ===== CONVERSION FUNCTIONS =====

  // Returns index of the last tempo change
  getTempoChangeFromTick(tick: number): number {
    for (let i = 0; i < this.#tempos.length; i++) {
      let change = this.#tempos[i];
      if (tick < change.tick) {
        return i - 1 < 0 ? 0 : i - 1;
      }
    }
    return this.#tempos.length - 1;
  }

  getTempoChangeMs(i: number) {
    if (i <= 0) {
      return this.#globalOffset;
    } else {
      let tempoValue = this.#tempos[i - 1].value;
      let previousTick = this.#tempos[i - 1].tick;
      let currentTick = this.#tempos[i].tick;

      let ticks = currentTick - previousTick;

      return (
        this.getTempoChangeMs(i - 1) +
        (this.tickToBeat(ticks) * tempoValue) / 1000
      );
    }
  }

  static tempoValueToBpm(value: number) {
    return 60000000 / value;
  }

  static bpmToTempoValue(bpm: number) {
    return 60000000 / bpm;
  }

  tickToBeat(tick: number) {
    return tick / this.#timeBase;
  }

  beatToMs(beat: number) {
    let currentTempoChangeIndex = this.getTempoChangeFromTick(
      this.beatToTick(beat)
    );
    let offset = this.getTempoChangeMs(currentTempoChangeIndex);

    let currentTempoChange = this.#tempos[currentTempoChangeIndex];
    let startTick = currentTempoChange.tick;
    let tempoValue = currentTempoChange.value;
    let beats = beat - this.tickToBeat(startTick);

    return (beats * tempoValue) / 1000 + offset;
  }

  tickToMs(tick: number) {
    return this.beatToMs(this.tickToBeat(tick));
  }

  beatToTick(beat: number) {
    return beat * this.#timeBase;
  }

  msToBeat(ms: number) {
    let offset = this.#globalOffset;
    let newOffset = offset;
    let previousChange = this.#tempos[0];

    for (let i = 1; i < this.#tempos.length; i++) {
      let tempoChange = this.#tempos[i];
      let ticks = tempoChange.tick - previousChange.tick;
      let beats = this.tickToBeat(ticks);

      newOffset += beats * (previousChange.value / 1000);

      if (newOffset >= ms) {
        break;
      }
      offset = newOffset;
      previousChange = tempoChange;
    }

    return (
      ((ms - offset) * 1000) / previousChange.value +
      this.tickToBeat(previousChange.tick)
    );
  }

  msToTick(ms: number) {
    return this.beatToTick(this.msToBeat(ms));
  }



  // ===== SEARCH FUNCTIONS =====

  // [TODO] actually get the note by its id and not by its index
  // or ensure that note id is always equal to its index
  getNoteById(id: number) {
    return this.getNote(id);
  }

  // Returns the page index
  getPageFromTick(tick: number): number | null {
    // Binary search

    if (tick < 0) return null;
    if (tick >= this.#pages[this.pageCount - 1].endTick) return null;

    let lower = 0;
    let upper = this.#pages.length - 1;

    while (lower != upper) {
      let middle = Math.floor((lower + upper) / 2);
      let page = this.#pages[middle];
      if (tick < page.startTick) {
        upper = middle - 1;
      } else if (tick >= page.endTick) {
        lower = middle + 1;
      } else {
        return middle;
      }
    }

    return lower;
  }

  isPageBoundary(tick: number) {
    let page = this.getPage(this.getPageFromTick(tick));
    return page.startTick === tick;
  }



  // ===== TRANSFORMATION FUNCTIONS =====
  // (converting tick to y position and vice versa)

  static getXFromNoteX(x: number) {
    let s = App.i.style;
    return x * s.playfieldInnerWidth + s.playfieldPadding;
  }

  static getNoteXFromX(x: number) {
    let s = App.i.style;
    return (x - s.playfieldPadding) / s.playfieldInnerWidth;
  }



  static getYFromTickHighway(tick: number, yOffset?: number) {
    if (yOffset === undefined) yOffset = style.linePos;
    return (
      R.i.canvas.height - (tick - state.currentTick) * state.zoom - yOffset
    );
  }

  getYFromTickGameplay(tick: number, page: Page | null, piano: boolean = false): number {
    if (piano) {
      // Highway position relative to scanline
      return Chart.getYFromTickHighway(tick, R.i.canvas.height - this.getYFromTick(state.currentTick, state.currentPage));
    }

    if (page === null) return 0;

    let progress = (tick - page.startTick) / (page.endTick - page.startTick);
    
    if (page.direction === 1) {
      progress = 1 - progress;
    }

    const positionFunction = page.positionFunction;
    if (positionFunction !== null) {
      // Transform from a number from 0 to 1 to a number from -1 to 1
      progress = (progress * 2) - 1;

      progress *= positionFunction.pageLength;  
      progress -= positionFunction.pageCenter; 

      // Transform back to a number from 0 to 1
      progress = (progress + 1) / 2;
    }

    return (
      (R.i.canvas.height - 2 * style.verticalMargin) * progress + style.verticalMargin
    );
  }

  getYFromTick(tick: number, page: Page | null, piano: boolean = false): number {
    return state.gameplayMode
      ? this.getYFromTickGameplay(tick, page, piano)
      : Chart.getYFromTickHighway(tick);
  }



  static getTickFromYHighway(y: number, yOffset = style.linePos) {
    return (R.i.canvas.height - yOffset - y) / state.zoom + state.currentTick;
  }

  getTickFromYGameplay(y: number, page: Page | null, piano: boolean = false) {
    if (piano) {
      return Chart.getTickFromYHighway(y, style.verticalMargin);
    }

    if (page === null) return 0;

    let progress =
      (y - style.verticalMargin) / (R.i.canvas.height - 2 * style.verticalMargin);
    if (page.direction === 1) {
      progress = 1 - progress;
    }

    return page.startTick + progress * (page.endTick - page.startTick);
  }

  getTickFromY(y: number, page: Page | null, piano: boolean = false) {
    return state.gameplayMode
      ? this.getTickFromYGameplay(y, page, piano)
      : Chart.getTickFromYHighway(y);
  }



  static getNotePositionHighway(note: Note) {
    let x = Chart.getXFromNoteX(note.x);
    let y = Chart.getYFromTickHighway(note.tick);
    return [x, y];
  }

  getNotePositionGameplay(note: Note, page: Page | null) {
    if (page === null) page = this.getPage(note.truePageIndex);
    let x = Chart.getXFromNoteX(note.x);
    let y = this.getYFromTickGameplay(note.tick, page, isPiano(note.type));
    return [x, y];
  }

  getNotePosition(note: Note, page: Page | null) {
    return state.gameplayMode
      ? this.getNotePositionGameplay(note, page)
      : Chart.getNotePositionHighway(note);
  }



  // ===== SNAPPING FUNCTIONS =====

  static snapXToGrid(x: number) {
    return Math.round(x * Settings.gridX) / Settings.gridX;
  }

  snapTickToGrid(tick: number, pageIndex: number | null = this.getPageFromTick(tick)) {
    if (pageIndex === null) {
      if (tick <= this.#pages[0].startTick) {
        pageIndex = 0;
      } else if (tick >= this.#pages[this.#pages.length - 1].endTick) {
        pageIndex = this.#pages.length - 1;
      } else {
        return tick;
      }
    }
    let page = this.#pages[pageIndex];
    let ticksPerSnap = this.#timeBase / Settings.gridY;
    let snapNumber = Math.round((tick - page.startTick) / ticksPerSnap);
    return page.startTick + snapNumber * ticksPerSnap;
  }




  // ===== CHART MODIFICATION FUNCTIONS =====

  private moveIds(from: number, offset: number) {
    for (let i = 0; i < this.#notes.length; i++) {
      if (this.#notes[i].id >= from) {
        this.#notes[i].id += offset;
      }
      if (this.#notes[i].nextId >= from) {
        if (this.#notes[i].nextId !== 0) {
          this.#notes[i].nextId += offset;
        }
      }
    }
  }

  private findPreviousNoteId(tick: number) {
    let lastId = 0;
    for (let x of this.#notes) {
      if (tick >= x.tick) {
        lastId = x.id;
      }
    }
    return lastId + 1;
  }

  addNote(newNote: Note) {
    let note = newNote.copy();
    note.id = this.findPreviousNoteId(note.tick);

    if (!this.isPageBoundary(note.tick)) {
      note.isForward = false;
    }
    if (isDrag(note.type)) {
      note.nextId = -1;
    }

    this.moveIds(note.id, 1);
    this.#notes.splice(note.id, 0, note);
    console.log("Added a new note", note);

    R.i.setDrawPlayfield();
    
    for (const note of this.#notes) {
      note.updateHasSibling(this);
      note.pageIndex = this.getPageFromTick(note.tick);
    }
  }

  removeNote(noteId: number) {
    for (let note of this.#notes) {
      if (note.nextId === noteId && note.nextId !== 0) {
        note.nextId = -1;
      }
    }

    this.moveIds(noteId, -1);
    this.#notes.splice(noteId, 1);
    console.log(`Removed note #${noteId}`);

    R.i.setDrawPlayfield();

    for (const note of this.#notes) {
      note.updateHasSibling(this);
      note.pageIndex = this.getPageFromTick(note.tick);
    }
  }

  pushPage(duration?: number) {
    const lastPage = this.#pages[this.#pages.length - 1];
    const startTick = lastPage.endTick;
    const endTick = (duration !== undefined)
      ? (startTick + duration)
      : (startTick + (lastPage.endTick - lastPage.startTick));

    const page = new Page(startTick, endTick, -lastPage.direction)
    this.#pages.push(page);
    console.log(`Appended new page #${this.#pages.length - 1}: ${page}`);
  }

  popPage() {
    this.#pages.pop();
    for (let i = 0; i < this.#notes.length; i++) {
      if (this.getNoteById(i).pageIndex === this.#pages.length) {
        this.removeNote(i);
        i--;
      }
    }
    console.log(`Removed last page`);
  }
}

export default Chart;
