import Util from "../util/Util.js";

class PositionFunction {
  #type: number;
  #arguments: number[];

  get type(): number {
    return this.#type;
  }

  // 1.0 = page spans whole height, 0.5 = page spans half the height
  get pageLength(): number {
    return <number> this.#arguments[0];
  }

  // 0.0 = page's center is in the middle of the screen, -0.5 page's center is halfway between the middle and the bottom of the screen
  get pageCenter(): number {
    return <number> this.#arguments[1];
  }



  static fromC2(c2data: any) {
    return new PositionFunction(
      Util.assertType("number", c2data.Type),
      Util.assertArray(c2data.Arguments).map(x => Util.assertType("number", x))
    );
  }

  toC2() {
    return {
      Type: this.#type,
      Arguments: this.#arguments
    };
  }

  static fromC2Min(c2mindata: any) {
    return c2mindata === undefined ? undefined : new PositionFunction(
      0,
      [
        Util.assertTypeOrDefault("number", c2mindata.l, 1.0),
        Util.assertTypeOrDefault("number", c2mindata.c, 0.0)
      ]
    );
  }

  toC2Min() {
    if (this.pageLength === 1.0 && this.pageCenter === 0.0) return undefined;
    return {
      l: this.pageLength === 1.0 ? undefined : this.pageLength,
      c: this.pageCenter === 0.0 ? undefined : this.pageCenter
    };
  }

  constructor(type = 0, arguments_: number[] = [1.0, 0.0]) {
    this.#type = type;
    if (arguments.length === 2) {
      this.#arguments = arguments_;
    } else {
      this.#arguments = [1.0, 0.0];
    }
  }
}

export default PositionFunction;
