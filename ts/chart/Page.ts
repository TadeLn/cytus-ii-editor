import Util from "../util/Util.js";
import PositionFunction from "./PositionFunction.js";

class Page {
  #startTick: number;
  #endTick: number;
  #direction: number;
  #positionFunction: PositionFunction | null;

  get startTick() {
    return this.#startTick;
  }

  get endTick() {
    return this.#endTick;
  }

  get direction() {
    return this.#direction;
  }

  get positionFunction() {
    return this.#positionFunction;
  }



  static fromC2(c2data: any) {
    return new Page(
      Util.assertType("number", c2data.start_tick),
      Util.assertType("number", c2data.end_tick),
      Util.assertType("number", c2data.scan_line_direction),
      c2data.PositionFunction !== undefined
        ? PositionFunction.fromC2(c2data.PositionFunction)
        : null
    );
  }

  toC2(formatVersion: number) {
    return {
      start_tick: this.#startTick,
      end_tick: this.#endTick,
      scan_line_direction: this.#direction,
      PositionFunction: formatVersion >= 1
        ? (this.#positionFunction === null ? new PositionFunction().toC2() : this.#positionFunction.toC2())
        : undefined
    };
  }

  static fromC2Min(c2mindata: any, lastPageEnd: number) {
    return new Page(
      lastPageEnd,
      Util.assertType("number", c2mindata.t),
      Util.assertTypeOrDefault("number", c2mindata.d, -1), // 1 and undefined instead of 1 and -1
      PositionFunction.fromC2Min(c2mindata.f)
    );
  }

  toC2Min() {
    return {
      // Start tick can be inferred from the previous page
      t: this.#endTick,
      d: this.#direction === 1 ? 1 : undefined, // 1 and undefined instead of 1 and -1
      f: this.#positionFunction === null ? undefined : this.#positionFunction.toC2Min()
    };
  }

  constructor(startTick = 0, endTick = 0, direction = 1, positionFunction: PositionFunction | null = null) {
    this.#startTick = startTick;
    this.#endTick = endTick;
    if (direction === -1 || direction === 1) {
      this.#direction = direction;
    } else {
      this.#direction = 1;
    }
    this.#positionFunction = positionFunction;
  }
}

export default Page;
