import App from "../App.js";
import GETRequest from "../util/request/GETRequest.js";
import { getDiffName } from "./Difficulty.js";

class ChartList {
  // [TODO] add type annotations
  static chartList = {};

  static hasDiff(diff: any) {
    let info = this.chartList["songs"][App.i.currentChart.filename];
    if (info === undefined) {
      return false;
    }

    return info.charts.includes(diff);
  }

  static getDiffByIndex(i: number) {
    let info = this.chartList["songs"][App.i.currentChart.filename];
    if (info === undefined) {
      return undefined;
    }

    let result = info.charts[i];
    if (result === undefined) {
      return undefined;
    }
    let resultInt = parseInt(result);
    if (isNaN(resultInt)) {
      return result;
    } else {
      return resultInt;
    }
  }

  static getMusicFilename(filename: string, diff: number) {
    let info = this.chartList["songs"][filename];

    if (info === undefined || info.musics === undefined) {
      return "song.ogg";
    }

    if (info.musics.includes(diff)) {
      return `song_${diff}.ogg`;
    }
    return "song.ogg"
  }

  static getSongDisplayName(filename: string) {
    const info = this.chartList["songs"][filename];
    if (info !== undefined) {
      if (info.title !== undefined) return info.title;
      if (info.id !== undefined) return info.id;
    }
    return filename;
  }

  static async init() {
    let links = document.getElementById("song-list");

    const req = new GETRequest(`./data/chartlist.json`);
    this.chartList = JSON.parse(await req.send());

    const songs = this.chartList["songs"];
    const collections = this.chartList["collections"];
    const collectionIds = Object.keys(collections).sort();

    for (let i = 0; i < collectionIds.length; i++) {
      const collectionId = collectionIds[i];
      const collection = collections[collectionId];
      const filenames = collection.sort();

      let collectionLabelDiv = document.createElement("div");
      collectionLabelDiv.classList.add("collection-label");
      collectionLabelDiv.id = `songlist-collectionlabel-${collectionId}`;

      let titleLabel = document.createElement("div");
      titleLabel.classList.add("title-label");
      {
        let title = document.createElement("span");
        title.classList.add("title");
        title.innerText = collectionId;
        titleLabel.appendChild(title);
      }
      collectionLabelDiv.appendChild(titleLabel);

      links.appendChild(collectionLabelDiv);

      let collectionDiv = document.createElement("div");
      collectionDiv.id = `songlist-collection-${collectionId}`;
      collectionDiv.classList.add("collection");

      for (let j = 0; j < filenames.length; j++) {
        let filename = filenames[j];
        let info = songs[filename];

        let songDiv = document.createElement("div");
        songDiv.classList.add("song");
        songDiv.id = `songlist-song-${filename}`;

        let anchor = document.createElement("a");
        anchor.classList.add("anchor");
        anchor.id = `songlist-anchor-${filename}`;
        songDiv.appendChild(anchor);

        let titleLabel = document.createElement("div");
        titleLabel.classList.add("title-label");
        {

          let column1 = document.createElement("div");
          {
            let title = document.createElement("span");
            title.classList.add("title");
            title.innerText = this.getSongDisplayName(filename);
            column1.appendChild(title);
              
            if (info.artist !== undefined) {
              let artist = document.createElement("span");
              artist.classList.add("artist");
              artist.innerText = info.artist;
              column1.appendChild(artist);
            }
          }
          titleLabel.appendChild(column1);

          let column2 = document.createElement("div");
          {
            if (info.id !== undefined) {
              let id = document.createElement("span");
              id.classList.add("id");
              id.innerText = info.id;
              column2.appendChild(id);
            }
          }
          titleLabel.appendChild(column2);
        }

        songDiv.appendChild(titleLabel);

        let difficulties = document.createElement("div");
        difficulties.classList.add("difficulties");

        if (info.charts === undefined) {
          info.charts = [];
        }
        info.charts.sort();

        for (let k = 0; k < info.charts.length; k++) {
          const difficultyId = info.charts[k];

          let a = document.createElement("a");
          a.id = `songlist-difficulty-${filename}-${difficultyId}`;
          a.classList.add("difficulty");
          a.classList.add(`difficulty-${difficultyId}`);

          let difficultyLabel = getDiffName(difficultyId);
          if (info.difficulties !== undefined && info.difficulties[difficultyId] !== undefined && info.difficulties[difficultyId] !== null) {
            difficultyLabel += ` [${info.difficulties[difficultyId]}]`;
          }
          if (Array.isArray(info.musics) && info.musics.includes(difficultyId)) {
            difficultyLabel += ` ♪`;
          }

          a.innerText = difficultyLabel;
          a.href = `#s#${filename}#${difficultyId}`;
          difficulties.appendChild(a);
        }
        songDiv.appendChild(difficulties);

        collectionDiv.appendChild(songDiv);
      }
      links.appendChild(collectionDiv);
    }
  }
}

export default ChartList;
