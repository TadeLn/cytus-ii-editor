import Util from "../util/Util.js";
import Event from "./event/Event.js";
import EventFactory from "./event/EventFactory.js";

class EventGroup {
  #tick: number;
  #events: Array<Event>;

  get tick() {
    return this.#tick;
  }

  get events() {
    return this.#events;
  }



  static fromC2(c2data: any) {
    return new EventGroup(
      Util.assertType("number", c2data.tick),
      Util.assertArray(c2data.event_list).map((x: any) => EventFactory.fromC2(x))
    );
  }

  toC2() {
    return {
      tick: this.#tick,
      event_list: this.#events.map(x => x.toC2()),
    };
  }

  static fromC2Min(c2mindata: any) {
    return new EventGroup(
      Util.assertTypeOrDefault("number", c2mindata.t, 0),
      Util.assertArray(c2mindata.e).map((x: any) => EventFactory.fromC2Min(x))
    );
  }

  toC2Min() {
    return {
      t: this.#tick === 0 ? undefined : this.#tick,
      e: this.#events.map(x => x.toC2Min()),
    };
  }

  constructor(tick = 0, events: Array<Event> = []) {
    this.#tick = tick;
    this.#events = events;
  }
}

export default EventGroup;
