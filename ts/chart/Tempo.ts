import Util from "../util/Util.js";

class Tempo {
  #tick: number;
  #value: number;

  get tick() {
    return this.#tick;
  }

  get value() {
    return this.#value
  }

  static fromC2(c2data: any) {
    return new Tempo(
      Util.assertType("number", c2data.tick),
      Util.assertType("number", c2data.value)
    );
  }

  toC2() {
    return {
      tick: this.#tick,
      value: this.#value,
    };
  }

  static fromC2Min(c2data: any) {
    return new Tempo(
      Util.assertTypeOrDefault("number", c2data.t, 0),
      Util.assertTypeOrDefault("number", c2data.v, 500000)
    );
  }

  toC2Min() {
    return {
      t: this.#tick  === 0      ? undefined : this.#tick,
      v: this.#value === 500000 ? undefined : this.#value,
    };
  }

  constructor(tick = 0, value = 0) {
    this.#tick = tick;
    this.#value = value;
  }
}

export default Tempo;
