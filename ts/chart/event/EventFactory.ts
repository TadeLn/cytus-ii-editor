import Util from "../../util/Util.js";
import Event, { EventType } from "./Event.js";
import FadeInUIEvent from "./FadeInUIEvent.js";
import FadeOutUIEvent from "./FadeOutUIEvent.js";
import DestroyUIEvent from "./AnimationOutUI.js";
import HideUIEvent from "./HideUIEvent.js";
import BuildUIEvent from "./AnimationInUI.js";
import ShowUIEvent from "./ShowUIEvent.js";
import SlowDownEvent from "./SlowDownEvent.js";
import SpeedUpEvent from "./SpeedUpEvent.js";
import TextEvent from "./TextEvent.js";
import UnknownEvent from "./UnknownEvent.js";

export default class EventFactory {

  static fromC2(c2data: any) {
    return EventFactory.makeEvent(
      Util.assertType("number", c2data.type),
      Util.assertType("string", c2data.args)
    );
  }

  static fromC2Min(c2mindata: any) {
    return EventFactory.makeEvent(
      Util.assertTypeOrDefault("number", c2mindata.t, 0),
      Util.assertTypeOrDefault("string", c2mindata.a, "")
    );
  }

  static makeEvent(type: EventType, args: string): Event {
    switch (type) {
      case EventType.SPEED_UP: return SpeedUpEvent.fromArgs(args);
      case EventType.SLOW_DOWN: return SlowDownEvent.fromArgs(args);
      case EventType.SHOW_UI: return ShowUIEvent.fromArgs(args);
      case EventType.HIDE_UI: return HideUIEvent.fromArgs(args);
      case EventType.FADE_IN_UI: return FadeInUIEvent.fromArgs(args);
      case EventType.FADE_OUT_UI: return FadeOutUIEvent.fromArgs(args);
      case EventType.ANIMATION_IN_UI: return BuildUIEvent.fromArgs(args);
      case EventType.ANIMATION_OUT_UI: return DestroyUIEvent.fromArgs(args);
      case EventType.TEXT: return TextEvent.fromArgs(args);
      default: return new UnknownEvent(type, args);
    }
  }

  private constructor() {}
}