import Color from "../../renderer/Color.js";
import { EventType } from "./Event.js";
import SpeedChangeEvent from "./SpeedChangeEvent.js";
import TextEvent from "./TextEvent.js";

export default class SpeedUpEvent extends SpeedChangeEvent {
  speedupType: string;

  static readonly #COLOR = Color.parseColor("#de595d");
  static get color() {
    return this.#COLOR.copy();
  }

  override get type() {
    return EventType.SPEED_UP;
  }

  override get args() {
    return this.speedupType;
  }

  override makeTextEvent(): TextEvent {
    return new TextEvent("SPEED UP", SpeedUpEvent.color);
  }
  
  override toString(): string {
    return `Speed Up: ${this.speedupType}`
  }

  static fromArgs(args: string): SpeedUpEvent {
    return new SpeedUpEvent(args);
  }

  constructor(speedupType: string) {
    super();
    this.speedupType = speedupType;
  }
}
