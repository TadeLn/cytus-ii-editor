import Event from "./Event.js";

export default class UIEvent extends Event {
  protected ui0 = false;  
  protected ui1 = false;
  protected ui2 = false;
  protected ui3 = false;
  protected ui4 = false;
  protected ui5 = false;
  protected ui6 = false;
  protected ui7 = false;

  #type: number;

  override get type(): number {
    return this.#type;
  }

  override get args() {
    return this.uiElements.map((x, i) => x ? i : undefined).join(",");
  }

  get uiElements(): boolean[] {
    return [
      this.ui0,
      this.ui1,
      this.ui2,
      this.ui3,
      this.ui4,
      this.ui5,
      this.ui6,
      this.ui7
    ];
  }

  uiElementsToString(): string {
    const names = [
      "Combo",
      "Score",
      "Title|difficulty",
      "Difficulty|title",
      "Scanline",
      "Boundary",
      "Visualizer",
      "ProgressBar"
    ];

    return this.uiElements.map((x, i) => x ? names[i] : undefined).filter(x => x !== undefined).join(", ");
  }

  protected static getFromArgs(args: string): boolean[] {
    let arr: boolean[] = [];
    
    args.split(",").forEach(x => {
      arr[x] = true;
    });
    
    return [
      arr[0] === true,
      arr[1] === true,
      arr[2] === true,
      arr[3] === true,
      arr[4] === true,
      arr[5] === true,
      arr[6] === true,
      arr[7] === true
    ];
  }

  protected constructor(type: number, ui0 = false, ui1 = false, ui2 = false, ui3 = false, ui4 = false, ui5 = false, ui6 = false, ui7 = false) {
    super();
    this.#type = type;
    this.ui0 = ui0;
    this.ui1 = ui1;
    this.ui2 = ui2;
    this.ui3 = ui3;
    this.ui4 = ui4;
    this.ui5 = ui5;
    this.ui6 = ui6;
    this.ui7 = ui7;
  }
}
