import Color from "../../renderer/Color.js";
import Event, { EventType } from "./Event.js";

export default class TextEvent extends Event {
  text: string;
  color: Color;

  override get type() {
    return EventType.TEXT;
  }

  override get args() {
    return `${this.text},${this.color}`;
  }
  
  override toString(): string {
    return `Text: ${this.text}, ${this.color.toString()}`
  }

  static fromArgs(args: string): TextEvent {
    const arr = args.split(",");
    let color = new Color(255, 255, 255);
    try {
      color = Color.parseColor(arr[1]);
    } catch (e) {
      console.warn("Invalid color code:", e);
    }
    return new TextEvent(arr[0], color);
  }

  constructor(text: string, color: Color) {
    super();
    this.text = text;
    this.color = color;
  }
}
