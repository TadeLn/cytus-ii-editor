import Color from "../../renderer/Color.js";
import { EventType } from "./Event.js";
import SpeedChangeEvent from "./SpeedChangeEvent.js";
import TextEvent from "./TextEvent.js";

export default class SlowDownEvent extends SpeedChangeEvent {
  speedupType: string;

  static readonly #COLOR = Color.parseColor("#5dde59");
  static get color() {
    return this.#COLOR.copy();
  }

  override get type() {
    return EventType.SPEED_UP;
  }

  override get args() {
    return this.speedupType;
  }

  override makeTextEvent(): TextEvent {
    return new TextEvent("SLOW DOWN", SlowDownEvent.color);
  }
  
  override toString(): string {
    return `Slow Down: ${this.speedupType}`
  }

  static fromArgs(args: string): SlowDownEvent {
    return new SlowDownEvent(args);
  }

  constructor(speedupType: string) {
    super();
    this.speedupType = speedupType;
  }
}
