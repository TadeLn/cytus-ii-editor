import { EventType } from "./Event.js";
import UIEvent from "./UIEvent.js";

export default class FadeOutUIEvent extends UIEvent {
  override toString(): string {
    return `Fade Out: ${this.uiElementsToString()}`;
  }

  static fromArgs(args: string): FadeOutUIEvent {
    return new FadeOutUIEvent(...super.getFromArgs(args));
  }

  constructor(ui0 = false, ui1 = false, ui2 = false, ui3 = false, ui4 = false, ui5 = false, ui6 = false, ui7 = false) {
    super(EventType.FADE_OUT_UI, ui0, ui1, ui2, ui3, ui4, ui5, ui6, ui7);
  }
}
