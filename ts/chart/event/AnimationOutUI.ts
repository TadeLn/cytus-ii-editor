import { EventType } from "./Event.js";
import UIEvent from "./UIEvent.js";

export default class DestroyUIEvent extends UIEvent {
  override toString(): string {
    return `Destroy: ${this.uiElementsToString()}`;
  }

  static fromArgs(args: string): DestroyUIEvent {
    return new DestroyUIEvent(...super.getFromArgs(args));
  }

  constructor(ui0 = false, ui1 = false, ui2 = false, ui3 = false, ui4 = false, ui5 = false, ui6 = false, ui7 = false) {
    super(EventType.ANIMATION_OUT_UI, ui0, ui1, ui2, ui3, ui4, ui5, ui6, ui7);
  }
}
