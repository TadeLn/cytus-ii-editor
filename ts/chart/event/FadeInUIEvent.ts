import { EventType } from "./Event.js";
import UIEvent from "./UIEvent.js";

export default class FadeInUIEvent extends UIEvent {
  override toString(): string {
    return `Fade In: ${this.uiElementsToString()}`;
  }

  static fromArgs(args: string): FadeInUIEvent {
    return new FadeInUIEvent(...super.getFromArgs(args));
  }

  constructor(ui0 = false, ui1 = false, ui2 = false, ui3 = false, ui4 = false, ui5 = false, ui6 = false, ui7 = false) {
    super(EventType.FADE_IN_UI, ui0, ui1, ui2, ui3, ui4, ui5, ui6, ui7);
  }
}
