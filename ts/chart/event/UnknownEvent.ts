import Color from "../../renderer/Color.js";
import Event, { EventType } from "./Event.js";

export default class UnknownEvent extends Event {
  #type: number;
  #args: string;

  override get type() {
    return this.#type;
  }

  override get args() {
    return this.#args;
  }
  
  override toString(): string {
    return `Unknown Event (${this.type}): ${this.args}`
  }

  constructor(type: number, args: string) {
    super();
    this.#type = type;
    this.#args = args;
  }
}
