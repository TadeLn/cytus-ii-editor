export enum EventType {
  SPEED_UP = 0,
  SLOW_DOWN = 1,
  SHOW_UI = 2,
  HIDE_UI = 3,
  FADE_IN_UI = 4,
  FADE_OUT_UI = 5,
  ANIMATION_IN_UI = 6,
  ANIMATION_OUT_UI = 7,
  TEXT = 8
}

export default abstract class Event {
  abstract get type(): number;
  abstract get args(): string;

  toString(): string {
    return `Event ${this.type}: ${this.args}`;
  }


  
  // fromC2 and fromC2Min methods are in EventFactory class

  toC2() {
    return {
      type: this.type,
      args: this.args,
    };
  }

  toC2Min() {
    return {
      t: this.type === 0  ? undefined : this.type,
      a: this.args === "" ? undefined : this.args,
    };
  }

  protected constructor() {}
}
