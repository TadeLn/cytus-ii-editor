import Event from "./Event.js";
import TextEvent from "./TextEvent.js";

export default abstract class SpeedChangeEvent extends Event {
  abstract makeTextEvent(): TextEvent;
}
