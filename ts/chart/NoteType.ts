enum NoteType {
  HIT_NOTE = 0,
  HOLD_NOTE = 1,
  HOLD_NOTE_LONG = 2,
  DRAG_NOTE = 3,
  DRAG_TICK = 4,
  SWIPE_NOTE = 5,
  HIT_DRAG_NOTE = 6,
  HIT_DRAG_TICK = 7,
  PIANO_NOTE = 8,
  PIANO_DRAG_NOTE = 9
};

export function getNoteName(type: NoteType) {
  switch (type) {
    case NoteType.HIT_NOTE:
      return "Hit Note";
    case NoteType.HOLD_NOTE:
      return "Hold Note";
    case NoteType.HOLD_NOTE_LONG:
      return "Hold Note (Long)";
    case NoteType.DRAG_NOTE:
      return "Drag Note";
    case NoteType.DRAG_TICK:
      return "Drag Tick";
    case NoteType.SWIPE_NOTE:
      return "Swipe Note";
    case NoteType.HIT_DRAG_NOTE:
      return "Hit Drag Note";
    case NoteType.HIT_DRAG_TICK:
      return "Hit Drag Tick";
    case NoteType.PIANO_NOTE:
      return "Piano Note";
    case NoteType.PIANO_DRAG_NOTE:
      return "Piano Drag Note";
  }
}

export function isHold(type: NoteType) {
  return type === NoteType.HOLD_NOTE || type === NoteType.HOLD_NOTE_LONG;
}

export function isDrag(type: NoteType) {
  return (
    type === NoteType.DRAG_NOTE ||
    type === NoteType.DRAG_TICK ||
    type === NoteType.HIT_DRAG_NOTE ||
    type === NoteType.HIT_DRAG_TICK
  );
}

export function isPiano(type: NoteType) {
  return (
    type === NoteType.PIANO_NOTE ||
    type === NoteType.PIANO_DRAG_NOTE
  );
}

export default NoteType;
