const difficultyNames = {
  0: "EASY",
  1: "HARD",
  2: "CHAOS",
  3: "GLITCH",
  4: "CRASH",
  5: "DREAM",
  6: "DROP"
};

function getDiffName(diff: string | number, showIndex = false) {
  let intDiff: number;
  if (typeof diff === "number") {
    intDiff = diff;
  } else {
    intDiff = parseInt(diff);
  }

  if (isNaN(intDiff)) {
    return `{${diff}} CUSTOM`;
  } else {
    if (difficultyNames[intDiff] !== undefined) {
      return showIndex
        ? `{${intDiff}} ${difficultyNames[intDiff]}`
        : difficultyNames[intDiff];
    } else {
      return `{${intDiff}} CUSTOM`;
    }
  }
}

export { difficultyNames, getDiffName };
