import App, { state, style } from "../App.js";
import Keyboard from "../input/Keyboard.js";
import Color from "../renderer/Color.js";
import { default as R } from "../renderer/Renderer.js";
import Settings from "../Settings.js";
import Util from "../util/Util.js";
import Chart from "./Chart.js";
import NoteType, { isDrag, isHold } from "./NoteType.js";

class Note {
  #pageIndex: number;
  #type: NoteType;
  #id: number;
  #tick: number;
  #x: number;
  #hasSibling: boolean;
  #duration: number;
  #nextId: number;
  #isForward: boolean;

  get pageIndex() {
    return this.#pageIndex;
  }

  get truePageIndex() {
    return this.pageIndex - (this.isForward ? 1 : 0)
  }

  get type() {
    return this.#type;
  }

  get id() {
    return this.#id;
  }

  get tick() {
    return this.#tick;
  }

  get x() {
    return this.#x;
  }

  get hasSibling() {
    return this.#hasSibling;
  }

  get duration() {
    return this.#duration;
  }

  get nextId() {
    return this.#nextId;
  }

  get isForward() {
    return this.#isForward
  }

  set pageIndex(pageIndex: number) {
    this.#pageIndex = pageIndex;
  }

  set type(type: NoteType) {
    if (!isHold(type)) {
      this.#duration = 0;
    }
    if (!isDrag(type)) {
      this.#nextId = 0;
    }
    this.#type = type;
  }

  set id(id: number) {
    this.#id = id;
  }

  set tick(tick: number) {
    const chart = App.i.currentChart;
    const oldTick = this.#tick;
    const prevNote = chart.getNoteById(this.id - 1);
    const nextNote = chart.getNoteById(this.id + 1);

    this.#tick = tick;
    this.updateHasSibling(chart);
    
    if (prevNote !== null) {
      if (prevNote.tick === oldTick) {
        prevNote.updateHasSibling(chart);
      }
    }

    if (nextNote !== null) {
      if (nextNote.tick === oldTick) {
        nextNote.updateHasSibling(chart);
      }
    }
  }

  set x(x: number) {
    this.#x = x;
  }

  set duration(duration: number) {
    if (isHold(this.type)) {
      if (duration > 0) {
        this.#duration = duration;
      } else {
        this.#duration = 0;
      }
    }
  }

  set nextId(nextId: number) {
    if (isDrag(this.type)) {
      if (nextId >= -1) {
        this.#nextId = nextId;
      }
    }
  }

  set isForward(isForward: boolean) {
    this.#isForward = isForward;
  }
  


  updateHasSibling(chart: Chart): void {
    this.#hasSibling = false;

    const prevNote = chart.getNoteById(this.id - 1);
    if (prevNote !== null) {
      if (prevNote.tick === this.tick) {
        this.#hasSibling = true;
      }
    }

    const nextNote = chart.getNoteById(this.id + 1);
    if (nextNote !== null) {
      if (nextNote.tick === this.tick) {
        this.#hasSibling = true;
      }
    }
  }



  static fromC2(c2data: any) {
    return new Note(
      Util.assertType("number", c2data.page_index),
      Util.assertType("number", c2data.type),
      Util.assertType("number", c2data.id),
      Util.assertType("number", c2data.tick),
      Util.assertType("number", c2data.x),
      Util.assertType("boolean", c2data.has_sibling),
      Util.assertType("number", c2data.hold_tick),
      Util.assertType("number", c2data.next_id),
      Util.assertType("boolean", c2data.is_forward)
    );
  }

  toC2() {
    return {
      page_index: this.#pageIndex,
      type: this.#type,
      id: this.#id,
      tick: this.#tick,
      x: this.#x,
      has_sibling: this.#hasSibling,
      hold_tick: this.#duration,
      next_id: this.#nextId,
      is_forward: this.#isForward,
    };
  }

  static fromC2Min(c2mindata: any, noteId: number) {
    return new Note(
      null,
      Util.assertTypeOrDefault("number", c2mindata.a, 0),
      noteId,
      Util.assertTypeOrDefault("number", c2mindata.t, 0),
      Util.assertTypeOrDefault("number", c2mindata.x, 0) / 1000,
      false,
      Util.assertTypeOrDefault("number", c2mindata.d, 0),
      Util.assertTypeOrDefault("number", c2mindata.n, 0),
      Util.assertTypeOrDefault("number", c2mindata.f, 0) === 1
    );
  }

  toC2Min() {
    return {
      a: this.type     === 0 ? undefined : this.type,
      t: this.tick     === 0 ? undefined : this.tick,
      x: this.x        === 0 ? undefined : Math.round(this.x * 1000),
      d: this.duration === 0 ? undefined : this.id,
      n: this.nextId   === 0 ? undefined : this.nextId,
      f: !this.isForward     ? undefined : 1,
    };
  }



  copy() {
    return new Note(
      this.pageIndex,
      this.type,
      this.id,
      this.tick,
      this.x,
      this.hasSibling,
      this.duration,
      this.nextId,
      this.isForward
    )
  }

  constructor(
    pageIndex = -1,
    type = NoteType.HIT_NOTE,
    id = -1,
    tick = 0,
    x = 0,
    hasSibling = false,
    duration = 0,
    nextId = 0,
    isForward = false
  ) {
    this.#pageIndex = pageIndex;
    this.#type = type;
    this.#id = id;
    this.#tick = tick;
    this.#x = x;
    this.#hasSibling = hasSibling;
    this.#duration = duration;
    this.#nextId = nextId;
    this.#isForward = isForward;
  }



  draw(
    x: number,
    y: number,
    pageIndex: number,
    active: number,
    hit: boolean | null = null,
    forceDisplay = false
  ) {
    const ctx = R.i.ctx;
    const chart = App.i.currentChart;
    const page = chart.getPage(pageIndex);
    const direction = page !== null ? page.direction : 1;
    // [TODO] change color based on direction

    const defaultRadius = style.noteRadius * active;
    const dragArrowRadius = style.dragArrowRadius * defaultRadius;
    const dragTickRadius = style.dragTickRadius * defaultRadius;
    const opacity = Settings.groupedMode
      ? Util.limitRange(0, 1, active * 1.5 - 0.5)
      : active;

    const defaultLineWidth = style.noteRadius * style.defaultLineWidth;
    const dragLineWidth = style.noteRadius * style.dragLineWidth;
    const pianoNoteLineWidth = style.noteRadius * style.pianoNoteLineWidth;

    const holdRectWidth = defaultRadius * style.holdRectWidth;
    const pianoNoteWidth = style.noteRadius * style.pianoNoteWidth;
    const pianoNoteThickness = style.noteRadius * style.pianoNoteThickness;

    // Automatically determine if the note was already hit or not
    if (hit === null) {
      hit = this.tick + this.duration < state.currentTick;

      // Preserve the drag notes
      if (Settings.showDragNotes) {
        if (this.nextId !== 0 && this.nextId !== -1) {
          if (chart.getNoteById(this.nextId).tick >= state.currentTick) {
            hit = false;
          }
        }
      }
    }

    if (hit && state.gameplayMode && !forceDisplay) {
      return;
    }

    const holdRectColor = style.holdRectColor.copy();
    holdRectColor.opacity = opacity;

    const pastNoteColor = style.pastNoteColor.copy();
    pastNoteColor.opacity = opacity;

    const progressCircleColor = style.progressCircleColor.copy();
    progressCircleColor.opacity = opacity;

    const dragNoteLine = style.dragNoteLine.copy();
    dragNoteLine.opacity = opacity;

    const holdColor = hit ? pastNoteColor : holdRectColor;
    holdColor.opacity = opacity;



    const hitNoteFill = style.hitNoteFill.copy();
    hitNoteFill.opacity = opacity;

    const hitNoteStroke = hit ? pastNoteColor : style.hitNoteStroke.copy();
    hitNoteStroke.opacity = opacity;

    const holdNoteFill = style.holdNoteFill.copy();
    holdNoteFill.opacity = opacity;

    const longHoldNoteFill = style.longHoldNoteFill.copy();
    longHoldNoteFill.opacity = opacity;

    const dragNoteFill = style.dragNoteFill.copy();
    dragNoteFill.opacity = opacity;

    const dragNoteStroke = hit ? pastNoteColor : style.dragNoteStroke.copy();
    dragNoteStroke.opacity = opacity;

    const swipeNoteFill = style.swipeNoteFill.copy();
    swipeNoteFill.opacity = opacity;

    const swipeNoteStroke = hit ? pastNoteColor : style.swipeNoteStroke.copy();
    swipeNoteStroke.opacity = opacity;

    const unknownNoteFill = style.unknownNoteFill.copy();
    unknownNoteFill.opacity = opacity;

    const unknownNoteStroke = hit ? pastNoteColor : style.unknownNoteStroke.copy();
    unknownNoteStroke.opacity = opacity;

    let text = `#${this.id}`;

    // Draw combo line
    if (Settings.showEvents) {
      if ((this.id + 1) % 25 === 0 && this.id > 0) {
        const y = chart.getYFromTick(this.tick + this.duration, chart.getPage(pageIndex));
        const fontSize = 30;

        R.i.drawEventLine(y, `rgba(255, 0, 0, ${opacity})`, [20, 10]);
        ctx.font = `${fontSize}px Electrolize`;
        ctx.fillStyle = `rgba(255, 0, 0, ${opacity})`;
        ctx.textAlign = "center";
        ctx.textBaseline = "bottom";
        R.i.drawTextWithShadow(
          `${this.id + 1}x`,
          (R.i.canvas.width) / 2,
          y,
          opacity
        );
      }
    }

    if (this.hasSibling) {
      ctx.fillStyle = `rgba(48, 48, 48, ${opacity * 0.5})`;
      ctx.beginPath();
      ctx.arc(x, y, defaultRadius + 20, 0, 2 * Math.PI, false);
      ctx.fill();
    }

    if (this.isForward) {
      ctx.fillStyle = "#0000ff7f";
      ctx.fillRect(
        x - defaultRadius,
        y - defaultRadius,
        defaultRadius * 2,
        defaultRadius * 2
      );
    }


    // Draw hold rectangle
    if (this.duration != 0) {
      ctx.fillStyle = holdColor.toRGBA();
      if (state.gameplayMode) {
        const pageHeight = R.i.canvas.height - 2 * style.verticalMargin;

        if (this.type === NoteType.HOLD_NOTE) {
          let pageDuration = 480;
          let pageDirection = 1;
          if (page !== null) {
            pageDuration = page.endTick - page.startTick
            pageDirection = page.direction;
          };

          let length = pageHeight * (this.duration / pageDuration);
          if (page.positionFunction !== null) {
            length *= page.positionFunction.pageLength;
          }

          if (pageDirection === 1) {
            ctx.fillRect(
              x - holdRectWidth / 2,
              y - length,
              holdRectWidth,
              length
            );
          } else {
            ctx.fillRect(x - holdRectWidth / 2, y, holdRectWidth, length);
          }
        } else if (this.type === NoteType.HOLD_NOTE_LONG) {
          ctx.fillRect(
            x - holdRectWidth / 2,
            style.verticalMargin,
            holdRectWidth,
            pageHeight
          );
        }
      } else {
        let length = this.duration * state.zoom;
        ctx.fillRect(x - holdRectWidth / 2, y - length, holdRectWidth, length);
      }
    }


    // Draw drag note details
    if (this.nextId != 0) {
      let nextNote = chart.getNoteById(this.nextId);
      if (nextNote !== null) {
        const [beginX, beginY] = [x, y];
        let [currentX, currentY] = [beginX, beginY];
        const [nextX, nextY] = chart.getNotePosition(nextNote, null);

        if (this.tick < state.currentTick) {
          let progress = (state.currentTick - this.tick) / (nextNote.tick - this.tick);
          let drawHead = true;

          // Draw drag arrow
          if (progress > 1) {
            drawHead = false;
            progress = Util.limitRange(0, 1, progress);
          }

          currentX = x + progress * (nextX - x);
          currentY = y + progress * (nextY - y);

          if (drawHead) {
            ctx.fillStyle = dragNoteFill.toRGBA();
            ctx.strokeStyle = dragNoteStroke.toRGBA();
            ctx.lineWidth = defaultLineWidth;
            ctx.beginPath();
            ctx.arc(currentX, currentY, dragArrowRadius, 0, 2 * Math.PI, false);
            ctx.fill();
            ctx.stroke();
          }
        }

        // Draw drag line
        if (!state.isPlaying) {
          ctx.lineWidth = 5;
          ctx.strokeStyle = dragNoteStroke.toRGBA();
          ctx.beginPath();
          ctx.moveTo(beginX, beginY);
          ctx.lineTo(nextX, nextY);
          ctx.stroke();
        }

        ctx.lineWidth = dragLineWidth;
        ctx.strokeStyle = dragNoteLine.toRGBA();
        ctx.setLineDash([5, 5]);
        ctx.beginPath();
        ctx.moveTo(nextX, nextY);
        ctx.lineTo(currentX, currentY);
        ctx.stroke();

        ctx.setLineDash([]);
      }
    }



    // Draw the note itself
    if (this.type === NoteType.HIT_NOTE) {
      // Hit note
      ctx.fillStyle = hitNoteFill.toRGBA();
      ctx.strokeStyle = hitNoteStroke.toRGBA();
      ctx.lineWidth = defaultLineWidth;
      ctx.beginPath();
      ctx.arc(x, y, defaultRadius, 0, 2 * Math.PI, false);
      ctx.fill();
      ctx.stroke();

    } else if (this.type === NoteType.HOLD_NOTE) {
      // Hold note
      ctx.fillStyle = holdNoteFill.toRGBA();
      ctx.strokeStyle = holdColor.toRGBA();
      ctx.lineWidth = defaultLineWidth;
      ctx.beginPath();
      ctx.arc(x, y, defaultRadius, 0, 2 * Math.PI, false);
      ctx.fill();
      ctx.stroke();

    } else if (this.type === NoteType.HOLD_NOTE_LONG) {
      // Long hold note (yellow)
      ctx.fillStyle = longHoldNoteFill.toRGBA();
      ctx.strokeStyle = holdColor.toRGBA();
      ctx.lineWidth = defaultLineWidth;
      ctx.beginPath();
      ctx.arc(x, y, defaultRadius, 0, 2 * Math.PI, false);
      ctx.fill();
      ctx.stroke();

    } else if (this.type === NoteType.DRAG_NOTE) {
      // Drag note
      if (
        !state.gameplayMode ||
        this.tick >= state.currentTick ||
        forceDisplay
      ) {
        ctx.fillStyle = dragNoteFill.toRGBA();
        ctx.strokeStyle = dragNoteStroke.toRGBA();
        ctx.lineWidth = defaultLineWidth;
        ctx.beginPath();
        ctx.arc(x, y, defaultRadius, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.stroke();
      }

    } else if (this.type === NoteType.DRAG_TICK) {
      // Drag tick
      if (
        !state.gameplayMode ||
        this.tick >= state.currentTick ||
        forceDisplay
      ) {
        ctx.fillStyle = dragNoteFill.toRGBA();
        ctx.strokeStyle = dragNoteStroke.toRGBA();
        ctx.lineWidth = defaultLineWidth;
        ctx.beginPath();
        ctx.arc(x, y, dragTickRadius, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.stroke();
      }

    } else if (this.type === NoteType.SWIPE_NOTE) {
      // Swipe note
      ctx.fillStyle = swipeNoteFill.toRGBA();
      ctx.strokeStyle = swipeNoteStroke.toRGBA();
      ctx.lineWidth = defaultLineWidth;
      ctx.beginPath();
      ctx.moveTo(x, y - defaultRadius);
      ctx.lineTo(x + defaultRadius, y);
      ctx.lineTo(x, y + defaultRadius);
      ctx.lineTo(x - defaultRadius, y);
      ctx.closePath();
      ctx.fill();
      ctx.stroke();

    } else if (this.type === NoteType.HIT_DRAG_NOTE) {
      // Hit drag note
      if (
        !state.gameplayMode ||
        this.tick >= state.currentTick ||
        forceDisplay
      ) {
        ctx.fillStyle = hitNoteFill.toRGBA();
        ctx.strokeStyle = dragNoteStroke.toRGBA();
        ctx.lineWidth = defaultLineWidth;
        ctx.beginPath();
        ctx.arc(x, y, defaultRadius, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.stroke();
      }

    } else if (this.type === NoteType.HIT_DRAG_TICK) {
      // Hit drag tick
      if (
        !state.gameplayMode ||
        this.tick >= state.currentTick ||
        forceDisplay
      ) {
        ctx.fillStyle = hitNoteFill.toRGBA();
        ctx.strokeStyle = dragNoteStroke.toRGBA();
        ctx.lineWidth = defaultLineWidth;
        ctx.beginPath();
        ctx.arc(x, y, dragTickRadius, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.stroke();
      }

    } else if (this.type === NoteType.PIANO_NOTE) {
      // Piano hit note
      ctx.fillStyle = hitNoteFill.toRGBA();
      ctx.strokeStyle = hitNoteStroke.toRGBA();
      ctx.lineWidth = pianoNoteLineWidth;
      ctx.beginPath();
      ctx.rect(
        x - pianoNoteWidth / 2,
        y - pianoNoteThickness / 2,
        pianoNoteWidth,
        pianoNoteThickness
      );
      ctx.fill();
      ctx.stroke();

    } else if (this.type === NoteType.PIANO_DRAG_NOTE) {
      // Piano drag note
      ctx.fillStyle = dragNoteFill.toRGBA();
      ctx.strokeStyle = dragNoteStroke.toRGBA();
      ctx.lineWidth = pianoNoteLineWidth;
      ctx.beginPath();
      ctx.rect(
        x - pianoNoteWidth / 2,
        y - pianoNoteThickness / 2,
        pianoNoteWidth,
        pianoNoteThickness
      );
      ctx.fill();
      ctx.stroke();

    } else {
      // Unknown note
      ctx.fillStyle = unknownNoteFill.toRGBA();
      ctx.strokeStyle = unknownNoteStroke.toRGBA();
      ctx.lineWidth = defaultLineWidth;
      ctx.beginPath();
      ctx.arc(x, y, defaultRadius, 0, 2 * Math.PI, false);
      ctx.fill();
      ctx.stroke();
      text += ` [${this.type}]`;
    }


    if (this.duration != 0) {
      // Draw hold note progress circle
      if (state.currentTick >= this.tick && state.currentTick <= this.tick + this.duration) {
        ctx.strokeStyle = style.progressCircleColor.toRGBA();
        ctx.lineWidth = 10;
        ctx.beginPath();
        ctx.arc(x, y, defaultRadius + 10, - Math.PI / 2, (((state.currentTick - this.tick) / this.duration) * 2 - 0.5) * Math.PI, false);
        ctx.stroke();
      }
    }



    // Add extra info text 
    if (this.duration != 0) {
      text += ` +${this.duration}`;
    }

    if (this.nextId != 0) {
      if (this.nextId === -1) {
        text += ` -> X`;
      } else {
        text += ` -> #${this.nextId}`;
      }
    }

    if (Settings.extraInfo) {
      ctx.font = "20px Electrolize";
      ctx.fillStyle = "#fff";
      ctx.textAlign = "center";
      ctx.textBaseline = "bottom";
      R.i.drawTextWithShadow(text, x, y - defaultRadius);
    }

    // Add duration text
    if (this.duration != 0) {
      if (Settings.showDuration) {
        const fontSize = 25;
        ctx.font = `${fontSize}px Electrolize`;
        ctx.fillStyle = `rgba(255, 255, 255, ${opacity})`;
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        const number = Keyboard.shift ? this.duration : Math.floor(chart.tickToBeat(this.duration) * 1000) / 1000
        R.i.drawTextWithShadow(`${number}`, x, y, opacity, defaultRadius * 2);
      }
    }
  }
}

export default Note;
