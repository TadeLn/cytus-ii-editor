import Settings from "../Settings.js";

class MusicManager {
  static audio = new Audio();
  static #lastMusicTime = 0;
  static #lastMusicTimeTimestamp = Date.now();

  static toggleAudio() {
    this.updateVolume();
    if (this.audio.paused) {
      this.audio.playbackRate = 1;
      this.audio.play();
    } else {
      this.audio.pause();
    }
  }

  static setAudioSpeed(speed: number) {
    if (this.audio.playbackRate != speed) {
      this.audio.playbackRate = speed;
    }
  }

  static seek(ms: number) {
    this.audio.currentTime = ms / 1000;
  }

  static getTime(timestamp = Date.now()) {
    const multiplier = this.audio.paused ? 0 : this.audio.playbackRate;
    return (this.#lastMusicTime * 1000) + ((timestamp - this.#lastMusicTimeTimestamp) * multiplier);
  }

  static get source() {
    return this.audio.src;
  }



  static init() {
    this.updateVolume();
  }

  static updateVolume() {
    this.audio.volume = Settings.musicVolume;
  }

  static update() {
    if (!this.audio.paused) {
      if (this.audio.currentTime !== this.#lastMusicTime) {
        this.#lastMusicTimeTimestamp = Date.now();
        this.#lastMusicTime = this.audio.currentTime;
      }
    }
  }
}

export default MusicManager;
