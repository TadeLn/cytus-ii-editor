
import MusicManager from "./music/MusicManager.js";

class Settings {
  static readonly defaultGridX = 20;
  static readonly defaultGridY = 16;

  static groupedMode = false;
  static showDragNotes = true;
  static showGrid = true;
  static gridX = Settings.defaultGridX;
  static gridY = Settings.defaultGridY;
  static extraInfo = false;
  static showEvents = true;
  static showModifierKeys = true;
  static showInput = false;
  static showDuration = true;
  static defaultZoomExponent = -4;
  static defaultGameplayMode = true;

  static #musicVolume = 0.5;
  static get musicVolume() {
    return this.#musicVolume;
  }
  static set musicVolume(value) {
    this.#musicVolume = value;
    MusicManager.update();
  }

  // Scroll speed (for scrolling while stopped)
  // (ticks per ms)
  static scrollSpeed = 1.5;
}

export default Settings;
